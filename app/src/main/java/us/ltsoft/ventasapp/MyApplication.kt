package us.ltsoft.ventasapp

import android.app.Application
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.utils.SharedPref

class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        VentasDB.initialize(this)
        SharedPref.initialize(this)
    }

}