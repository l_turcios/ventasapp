package us.ltsoft.ventasapp.ui.fragments.productos

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.dto.PrecioDTO

class ProductoViewModel(productoId: String? = null) : ViewModel() {
    private val db: VentasDB = VentasDB.getInstance()
    var precios: LiveData<List<PrecioDTO>> = db.getProductoPreciosDao().getProductoPreciosByProductoId(productoId ?: "")
}