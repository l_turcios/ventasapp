package us.ltsoft.ventasapp.ui.fragments.ventas

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import androidx.lifecycle.ViewModel
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.utils.printer.PrintConstants
import us.ltsoft.ventasapp.utils.printer.PrintHistorialVenta
import us.ltsoft.ventasapp.utils.printer.PrintTicketVenta
import java.time.LocalDateTime

class VentasViewModel : ViewModel() {
    private val db = VentasDB.getInstance()
    var ventas = db.getVentasDao().getVentas().toMutableList()

    @SuppressLint("MissingPermission")
    fun imprimirHistorial(device: BluetoothDevice): Boolean {
        val socket: BluetoothSocket = device.createRfcommSocketToServiceRecord(PrintConstants.MY_UUID)
        var result = false
        val latch = java.util.concurrent.CountDownLatch(1)
        val printing = PrintHistorialVenta(
            socket, ventas, object : PrintHistorialVenta.OnPrintingCompleteListener {
            override fun onPrintingComplete() {
                latch.countDown()
                result = true
            }
            override fun onPrintingFailed(error: String) {
                latch.countDown()
                result = false
            }
        })
        printing.start()
        return try {
            latch.await()
            result
        } catch (e: InterruptedException) {
            e.printStackTrace()
            result
        }
    }

    fun filtrarVentasPorFecha(fecha: String){
        ventas = db.getVentasDao().getVentasDTOByFecha(fecha).toMutableList()
    }

    fun itemCount(): Int {
        return ventas.size
    }
}