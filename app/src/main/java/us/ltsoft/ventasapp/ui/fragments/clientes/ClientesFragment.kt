package us.ltsoft.ventasapp.ui.fragments.clientes

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.adapters.ClientesFragmentClientesAdapter
import us.ltsoft.ventasapp.adapters.OnClienteIntoFragmentClickListener
import us.ltsoft.ventasapp.data.firestore.FirestoreService
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO
import us.ltsoft.ventasapp.data.response.ClienteResponse
import us.ltsoft.ventasapp.databinding.FragmentClientesBinding
import us.ltsoft.ventasapp.ui.activities.main.MainActivity

class ClientesFragment : Fragment() {

    private var _binding: FragmentClientesBinding? = null
    private val binding get() = _binding!!
    private lateinit var firestoreService: FirestoreService
    private var db = VentasDB.getInstance()
    private lateinit var clientesViewModel: ClientesViewModel
    private lateinit var adapter: ClientesFragmentClientesAdapter
    private lateinit var recyclerView: RecyclerView
    private var clienteId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
        setFragmentResultListener("clienteId") { _, bundle ->
            val cliente = ClienteResponse(bundle.getString("clienteId")!!,
                bundle.getString("nombre")!!,
                bundle.getString("telefono")!!,
                bundle.getString("escalaId")!!)
            clientesViewModel.viewModelScope.launch {
                val sync =
                    firestoreService.addDocument(
                        "clientes",
                        cliente.id, cliente)
                if (sync) {
                    db.getClientesDao().updateClienteSincronizado(cliente.id)
                    Log.d("ClientesFragment", "Cliente sincronizado")
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentClientesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = binding.rvClientes
        clientesViewModel = ViewModelProvider(this)[ClientesViewModel::class.java]
        adapter = ClientesFragmentClientesAdapter(requireContext())

        clientesViewModel.clientes.observe(viewLifecycleOwner) { clientes ->
            (recyclerView.adapter as ClientesFragmentClientesAdapter).setClientes(clientes)
        }

        adapter.setOnClienteClickListener(object: OnClienteIntoFragmentClickListener {
            override fun onClienteClick(cliente: ClienteDTO) {
                clienteId = cliente.id
                if(clienteId!!.isNotEmpty()) {
                    val bundle = bundleOf("clienteId" to clienteId)
                    findNavController().navigate(R.id.action_nav_clientes_to_nav_cliente, bundle)
                }
            }

        })

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter

        binding.btnNuevoCliente.setOnClickListener {
            findNavController().navigate(R.id.action_nav_clientes_to_nav_cliente)
        }

    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)!!.setFabVisibility(false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}