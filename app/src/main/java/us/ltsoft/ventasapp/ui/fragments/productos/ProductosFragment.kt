package us.ltsoft.ventasapp.ui.fragments.productos

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.adapters.OnProductoIntoFragmentClickListener
import us.ltsoft.ventasapp.adapters.ProductosFragmentProductosAdapter
import us.ltsoft.ventasapp.data.firestore.FirestoreService
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.dto.ProductoDTO
import us.ltsoft.ventasapp.databinding.FragmentProductosBinding
import us.ltsoft.ventasapp.ui.activities.main.MainActivity

class ProductosFragment : Fragment() {

    private var _binding: FragmentProductosBinding? = null
    private val binding get() = _binding!!
    private lateinit var firestoreService: FirestoreService
    private var db = VentasDB.getInstance()
    private lateinit var viewModel: ProductosViewModel
    private lateinit var adapter: ProductosFragmentProductosAdapter
    private lateinit var recyclerView: RecyclerView
    private var productoId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
        setFragmentResultListener("productoId") { _, bundle ->
            val producto = bundle.getString("productoId")!!
            viewModel.viewModelScope.launch {
                val sync =
                    firestoreService.addDocument(
                        "productos",
                        producto, producto)
                if (sync) {
                    Log.d("ProductosFragment", "Producto sincronizado")
                    // db.getProductosDao().updateProductoSincronizado(producto)
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductosBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.rvProductos
        viewModel = ViewModelProvider(this)[ProductosViewModel::class.java]
        adapter = ProductosFragmentProductosAdapter(requireContext())

        viewModel.productos.observe(viewLifecycleOwner) {productos ->
            (recyclerView.adapter as ProductosFragmentProductosAdapter).setProductos(productos)
        }

        adapter.setOnProductoClickListener(object : OnProductoIntoFragmentClickListener {
            override fun onProductoClick(producto: ProductoDTO) {
                productoId = producto.id
                if (productoId!!.isNotEmpty()){
                    val bundle = Bundle()
                    bundle.putString("productoId", productoId)
                    findNavController().navigate(R.id.action_nav_productos_to_nav_producto, bundle)
                }
            }
        })

        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        binding.btnNuevoProducto.setOnClickListener {
            findNavController().navigate(R.id.action_nav_productos_to_nav_producto)
        }

    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)!!.setFabVisibility(false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}