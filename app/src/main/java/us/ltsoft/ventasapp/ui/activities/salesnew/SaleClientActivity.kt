package us.ltsoft.ventasapp.ui.activities.salesnew

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextWatcher
import androidx.recyclerview.widget.LinearLayoutManager
import us.ltsoft.ventasapp.adapters.ClientesNuevaVentaAdapter
import us.ltsoft.ventasapp.adapters.OnClienteClickListener
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.Venta
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO
import us.ltsoft.ventasapp.databinding.ActivitySaleClientBinding
import us.ltsoft.ventasapp.utils.SharedPref
import us.ltsoft.ventasapp.utils.Utils
import java.time.LocalDateTime

class SaleClientActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySaleClientBinding
    private lateinit var adapter: ClientesNuevaVentaAdapter
    private lateinit var db: VentasDB
    private lateinit var sharedPref: SharedPref
    private var cliente: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySaleClientBinding.inflate(layoutInflater)
        setContentView(binding.root)
        db = VentasDB.getInstance()
        sharedPref = SharedPref.getInstance()
        adapter = ClientesNuevaVentaAdapter(db.getClientesDao().getAllClientesDTO().value!!)

        adapter.setOnClienteClickListener(object: OnClienteClickListener {
            override fun onClienteClick(clienteSeleccionado: ClienteDTO) {
                cliente = clienteSeleccionado.id
                if(cliente!!.isNotEmpty()) {
                    binding.btnContinuarNuevaVentaConCliente.isEnabled = true
                }
            }
        })

        val recyclerView = binding.rvClientesNuevaVenta
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)

        val editTextBusucarCliente = binding.editTextBuscarClienteNuevaVenta
        editTextBusucarCliente.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                adapter.filter.filter(s)
            }

            override fun afterTextChanged(s: android.text.Editable?) {

            }
        })

        binding.btnContinuarNuevaVentaConCliente.setOnClickListener {
            if(cliente != null) {
                db.getVentasDao().upsertVenta(
                    Venta(
                        clienteId = cliente!!,
                        estado = "En proceso",
                        sincronizado = false,
                        usuarioId = sharedPref.userId!!,
                        total = 0.0,
                        fecha = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) LocalDateTime.now() else null,
                        id = Utils().generarCodigo()
                    )
                )
                finish()
            }
        }

    }
}