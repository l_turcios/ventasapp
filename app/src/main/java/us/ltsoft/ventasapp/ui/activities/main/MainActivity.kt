package us.ltsoft.ventasapp.ui.activities.main

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.lorenzofelletti.permissions.PermissionManager
import com.lorenzofelletti.permissions.dispatcher.dsl.checkPermissions
import com.lorenzofelletti.permissions.dispatcher.dsl.doOnDenied
import com.lorenzofelletti.permissions.dispatcher.dsl.doOnGranted
import com.lorenzofelletti.permissions.dispatcher.dsl.withRequestCode
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO
import us.ltsoft.ventasapp.databinding.ActivityMainBinding
import us.ltsoft.ventasapp.ui.activities.salesnew.SaleClientActivity
import us.ltsoft.ventasapp.ui.activities.salesnew.SaleProductActivity
import us.ltsoft.ventasapp.ui.fragments.home.FabActionListener

class MainActivity : AppCompatActivity(), FabActionListener {

    private val TAG: String? = MainActivity::class.simpleName
    private val permissionManager = PermissionManager(this)
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    lateinit var fab: FloatingActionButton
    // val navController = findNavController(R.id.nav_host_fragment_content_main)
    companion object {
        private const val BLUETOOTH_REQUEST_CODE = 1

        private val BLUETOOTH_REQUIRED_PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            arrayOf(
                android.Manifest.permission.BLUETOOTH,
                android.Manifest.permission.BLUETOOTH_ADMIN,
                android.Manifest.permission.BLUETOOTH_CONNECT,
            )
        } else {
            arrayOf(
                android.Manifest.permission.BLUETOOTH,
                android.Manifest.permission.BLUETOOTH_ADMIN
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        fab = binding.appBarMain.fab

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_ventas, R.id.nav_clientes, R.id.nav_productos
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        permissionManager.buildRequestResultsDispatcher {
            withRequestCode(BLUETOOTH_REQUEST_CODE) {
                checkPermissions(BLUETOOTH_REQUIRED_PERMISSIONS)
                doOnGranted {
                    Log.d(TAG, "Permission granted")
                }
                doOnDenied {
                    Log.d(TAG, "Permission denied")
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionManager.dispatchOnRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onFabVentasAction(isEmpty: Boolean, ventaId: String?, escalaId: String?) {
        if(isEmpty) {
            val intent = Intent(this, SaleClientActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, SaleProductActivity::class.java)
            intent.putExtra("ventaId", ventaId)
            intent.putExtra("escalaId", escalaId)
            startActivity(intent)
        }
    }

    override fun onFabClientesAction() {

    }

    fun setFabVisibility(isVisible: Boolean) {
        if(::binding.isInitialized) {
            binding.appBarMain.fab.visibility = if(isVisible) View.VISIBLE else View.INVISIBLE
        }
    }

}