package us.ltsoft.ventasapp.ui.fragments.home

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.adapters.SwipeToDeleteCallback
import us.ltsoft.ventasapp.adapters.VentasDetalleAdapter
import us.ltsoft.ventasapp.data.firestore.FirestoreService
import us.ltsoft.ventasapp.data.model.db.PersistData
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.dto.VentaDTO
import us.ltsoft.ventasapp.data.response.VentaResponse
import us.ltsoft.ventasapp.data.response.VentasDetalleResponse
import us.ltsoft.ventasapp.databinding.FragmentHomeBinding
import us.ltsoft.ventasapp.ui.activities.main.MainActivity
import us.ltsoft.ventasapp.utils.SharedPref
import us.ltsoft.ventasapp.utils.printer.BluetoothCheck
import java.time.ZoneId

interface FabActionListener {
    fun onFabVentasAction(isEmpty: Boolean, ventaId: String? = null, escalaId: String? = null)
    fun onFabClientesAction()
}

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private lateinit var fabActionListener: FabActionListener

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var firestoreService: FirestoreService
    private var persistData: PersistData = PersistData()

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var recyclerView: RecyclerView
    private var sharedPref: SharedPref = SharedPref.getInstance()
    private var db: VentasDB = VentasDB.getInstance()
    private var ventaEnProceso: VentaDTO? = null
    private var device: BluetoothDevice? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        firestoreService = FirestoreService(FirebaseFirestore.getInstance(), persistData)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        recyclerView = binding.rvVentaEnProcesoDetalle
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = VentasDetalleAdapter(requireContext())

        // Swipe to delete
        val itemTouchHelper =
            ItemTouchHelper(SwipeToDeleteCallback(recyclerView.adapter as VentasDetalleAdapter) {
                homeViewModel.eliminarItem(it)
            })

        itemTouchHelper.attachToRecyclerView(recyclerView)

        homeViewModel.ventaDetalle.observe(viewLifecycleOwner, Observer {
            (recyclerView.adapter as VentasDetalleAdapter).setVentasDetalle(it)
        })

        binding.textTotalVentaEnProceso.setOnClickListener {
            if (homeViewModel.itemCount() < 1) {
                Toast.makeText(requireContext(), "No hay productos en la venta", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }

            val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
            if (bluetoothAdapter == null) {
                Toast.makeText(
                    requireContext(),
                    "Bluetooth no soportado",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            device = BluetoothCheck(
                bluetoothAdapter,
                sharedPref,
                device,
                this@HomeFragment,
                requireContext()
            ).eval()

            if (device == null) {
                return@setOnClickListener
            }

            val sweetAlertDialog =
                SweetAlertDialog(requireContext(), SweetAlertDialog.WARNING_TYPE)
            sweetAlertDialog.setCancelable(false)
            sweetAlertDialog.titleText = "Imprimir"
            sweetAlertDialog.contentText = "¿Desea imprimir la venta?"
            sweetAlertDialog.confirmText = "Si"
            sweetAlertDialog.setConfirmClickListener {
                if (homeViewModel.imprimirVenta(device!!, ventaEnProceso!!.venta)) {
                    it.titleText = "Imprimiendo"
                    it.contentText = "¿Desea imprimir otro ticket?"
                    it.changeAlertType(SweetAlertDialog.SUCCESS_TYPE)
                    it.setCancelable(true)
                    it.confirmText = "Si"
                    it.setConfirmClickListener { another ->
                        if (homeViewModel.imprimirVenta(device!!, ventaEnProceso!!.venta)) {
                            cerrarVenta()
                            another.changeAlertType(SweetAlertDialog.SUCCESS_TYPE)
                            another.dismissWithAnimation()
                            onResume()
                        } else {
                            another.titleText = "Error"
                            another.contentText = "No se pudo imprimir el duplicado"
                            another.changeAlertType(SweetAlertDialog.ERROR_TYPE)
                            another.setCancelable(false)
                            another.confirmText = "Aceptar"
                            another.setConfirmClickListener { another2 ->
                                cerrarVenta()
                                another2.dismissWithAnimation()
                                onResume()
                            }
                        }
                    }
                    it.cancelText = "No"
                    it.showCancelButton(true)
                    it.setCancelClickListener { another ->
                        cerrarVenta()
                        another.changeAlertType(SweetAlertDialog.WARNING_TYPE)
                        onResume()
                        another.dismissWithAnimation()
                    }
                    it.show()
                } else {
                    it.titleText = "Error"
                    it.contentText = "No se pudo imprimir el ticket"
                    it.changeAlertType(SweetAlertDialog.ERROR_TYPE)
                    it.setCancelable(false)
                    it.cancelText = null
                    it.confirmText = "Aceptar"
                    it.setConfirmClickListener { another ->
                        another.changeAlertType(SweetAlertDialog.WARNING_TYPE)
                        another.titleText = "SOLO GUARDAR"
                        another.contentText = "¿Desea guardar esta venta SIN IMPRIMIR?"
                        another.confirmText = "Si"
                        another.setConfirmClickListener { another2 ->
                            cerrarVenta()
                            another2.changeAlertType(SweetAlertDialog.SUCCESS_TYPE)
                            onResume()
                            another2.dismissWithAnimation()
                        }
                        another.cancelText = "No"
                        another.setCancelClickListener { another2 ->
                            another2.dismissWithAnimation()
                        }
                        another.show()
                    }
                    it.show()
                }
            }
            sweetAlertDialog.cancelText = "No"
            sweetAlertDialog.setCancelClickListener {
                sweetAlertDialog.dismissWithAnimation()
            }
            sweetAlertDialog.show()


        }

    }

    override fun onResume() {
        super.onResume()
        ventaEnProceso = db.getVentasDao().getVentaEnProceso()
        if (ventaEnProceso != null) {
            // val totalVenta = db.getVentasDetalleDao().getTotalVenta(ventaEnProceso!!.venta)
            binding.textEtiquetaTotalVenta.text = ""
            binding.textEtiquetaTotalVenta.visibility = View.INVISIBLE
            binding.textTotalVentaEnProceso.visibility = View.VISIBLE
            binding.textTotalVentaEnProceso.text = String.format("$ %.2f", ventaEnProceso!!.total)
        } else {
            binding.textEtiquetaTotalVenta.text = getString(R.string.sin_venta_en_proceso)
            binding.textEtiquetaTotalVenta.visibility = View.VISIBLE
            binding.textTotalVentaEnProceso.visibility = View.INVISIBLE
        }
        (activity as? MainActivity?)!!.setFabVisibility(true)
        (activity as? MainActivity?)!!.fab.setOnClickListener {
            fabActionListener.onFabVentasAction(
                ventaEnProceso == null,
                ventaEnProceso?.venta,
                ventaEnProceso?.escalaId
            )
        }
        if (sharedPref.isThereNewSale) {
            Toast.makeText(
                requireContext(), getString(R.string.item_added_to_sale), Toast.LENGTH_SHORT
            )
                .show()
            sharedPref.isThereNewSale = false
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FabActionListener) {
            fabActionListener = context
        } else {
            Log.e("HomeFragment", "onAttach: $context must implement FabActionListener")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun cerrarVenta(): Boolean {
        val customId: String = ventaEnProceso!!.venta
        if (homeViewModel.cerrarVenta(customId)) {
            homeViewModel.viewModelScope.launch {
                val successMaster = firestoreService.addDocument(
                    "ventas",
                    customId,
                    VentaResponse(
                        customId,
                        ventaEnProceso!!.clienteId!!,
                        Timestamp(
                            ventaEnProceso!!.fecha!!.atZone(ZoneId.systemDefault()).toEpochSecond(),
                            ventaEnProceso!!.fecha!!.nano
                        ),
                        ventaEnProceso!!.total,
                        "Cerrada",
                        sharedPref.userId.toString()
                    )
                )
                if (successMaster) {
                    homeViewModel.sincronizadaVenta(customId)
                    for (item in db.getVentasDetalleDao().getVentasDetalleByVentaId(customId)) {
                        val successDetail = firestoreService.addDocument(
                            "ventasDetalle",
                            item.id,
                            VentasDetalleResponse(
                                item.id,
                                item.ventaId,
                                item.productoId,
                                item.cantidad,
                                item.precio,
                                item.total
                            )
                        )
                        if (successDetail) {
                            homeViewModel.sincronizadaVentaDetalle(item.id)
                        }
                    }
                }

            }
        }
        return true
    }

}