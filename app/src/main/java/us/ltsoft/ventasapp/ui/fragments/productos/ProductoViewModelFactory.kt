package us.ltsoft.ventasapp.ui.fragments.productos

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ProductoViewModelFactory(private val productoId: String? = null): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ProductoViewModel(productoId) as T
    }
}