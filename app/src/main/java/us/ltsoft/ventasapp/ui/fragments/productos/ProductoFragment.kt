package us.ltsoft.ventasapp.ui.fragments.productos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.adapters.OnPrecioIntoFragmentClickListener
import us.ltsoft.ventasapp.adapters.ProductoFragmentPreciosAdapter
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.Categoria
import us.ltsoft.ventasapp.data.model.entities.Producto
import us.ltsoft.ventasapp.data.model.entities.dto.PrecioDTO
import us.ltsoft.ventasapp.databinding.FragmentProductoBinding
import us.ltsoft.ventasapp.ui.activities.main.MainActivity

class ProductoFragment : Fragment() {
    private var _binding: FragmentProductoBinding? = null
    private val binding get() = _binding!!
    private val db = VentasDB.getInstance()
    private val categorias = db.getCategoriasDao().getAllCategorias()
    private var precioId: String? = null
    private var productoId: String? = null
    private var producto: Producto? = null
    private lateinit var categoria: Categoria
    private lateinit var productoViewModel: ProductoViewModel
    private lateinit var adapter: ProductoFragmentPreciosAdapter
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        productoId = arguments?.getString("productoId")
        _binding = FragmentProductoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkArguments()
        recyclerView = binding.rvPreciosProductoFragmentProducto
        productoViewModel = ViewModelProvider(this, ProductoViewModelFactory(productoId))[ProductoViewModel::class.java]
        adapter = ProductoFragmentPreciosAdapter(requireContext())

        productoViewModel.precios.observe(viewLifecycleOwner) { precios ->
            (recyclerView.adapter as ProductoFragmentPreciosAdapter).setPrecios(precios)
        }

        adapter.setOnPrecioClickListener(object : OnPrecioIntoFragmentClickListener {
            override fun onPrecioClick(precio: PrecioDTO) {
                precioId = precio.id
            }
        })

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.setHasFixedSize(true)

        binding.btnCancelarProductoFragmentProducto.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun checkArguments() {
        if (productoId != null) {
            producto = db.getProductosDao().getProducto(productoId!!)
            if(producto != null){
                categoria = db.getCategoriasDao().getCategoriaById(producto!!.categoriaId)
            }
        }
        updateView()
    }

    private fun updateView() {
        (activity as MainActivity).supportActionBar?.title = if (productoId != null) {
            "Editar producto"
        } else {
            "Nuevo producto"
        }
        binding.editNombreProductoFragmentProducto.setText(producto?.producto ?: "")
        binding.spinnerCategoriaProductoFragmentProducto.adapter = android.widget.ArrayAdapter(
            requireContext(),
            R.layout.item_spinner_list,
            categorias.map { it.categoria }
        )
        if (producto != null) {
            binding.spinnerCategoriaProductoFragmentProducto.setSelection(
                categorias.indexOfFirst { it.id == producto!!.categoriaId }
            )
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).setFabVisibility(false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}