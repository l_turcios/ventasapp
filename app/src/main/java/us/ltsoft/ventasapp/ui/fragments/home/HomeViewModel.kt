package us.ltsoft.ventasapp.ui.fragments.home

import android.annotation.SuppressLint
import android.app.Application
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.dto.VentasDetalleDTO
import us.ltsoft.ventasapp.utils.printer.PrintConstants
import us.ltsoft.ventasapp.utils.printer.PrintTicketVenta
import java.util.UUID

class HomeViewModel(application: Application): AndroidViewModel(application) {
    private val db = VentasDB.getInstance()
    val ventaDetalle: LiveData<List<VentasDetalleDTO>> = db.getVentasDetalleDao().getVentasDetalleEnProcesoDTO()

    fun eliminarItem(position: Int) {
        val listaActual = ventaDetalle.value?.toMutableList() ?: mutableListOf()
        if (position in 0 until listaActual.size) {
            db.getVentasDetalleDao().eliminarItem(listaActual[position].id)
        }
    }

    @SuppressLint("MissingPermission")
    fun imprimirVenta(device: BluetoothDevice, ventaId: String): Boolean {
        val socket: BluetoothSocket = device.createRfcommSocketToServiceRecord(PrintConstants.MY_UUID)
        val venta = db.getVentasDao().getVentaEnProcesoDTOById(ventaId)
        var result = false
        val latch = java.util.concurrent.CountDownLatch(1)
        val printing = PrintTicketVenta(socket, venta!!, ventaDetalle, object : PrintTicketVenta.OnPrintingCompleteListener {
            override fun onPrintingComplete() {
                latch.countDown()
                result = true
            }
            override fun onPrintingFailed(error: String) {
                latch.countDown()
                result = false
            }
        })
        printing.start()
        return try {
            latch.await()
            result
        } catch (e: InterruptedException) {
            e.printStackTrace()
            result
        }
    }

    fun cerrarVenta(ventaId: String): Boolean {
        var result: Boolean = db.getVentasDao().cerrarVenta(ventaId)
        Toast.makeText(getApplication(), "Venta cerrada", Toast.LENGTH_SHORT).show()
        return result
    }

    fun sincronizadaVenta(ventaId: String): Boolean {
        var result: Boolean = db.getVentasDao().sincronizadaVenta(ventaId)
        return result
    }
    fun sincronizadaVentaDetalle(ventaId: String): Boolean {
        var result: Boolean = db.getVentasDetalleDao().sincronizadaVentaDetalle(ventaId)
        return result
    }

    fun itemCount(): Int {
        return ventaDetalle.value?.size ?: 0
    }
}