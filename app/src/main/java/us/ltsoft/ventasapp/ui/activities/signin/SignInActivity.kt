package us.ltsoft.ventasapp.ui.activities.signin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import us.ltsoft.ventasapp.ui.activities.main.MainActivity
import us.ltsoft.ventasapp.data.firestore.FirestoreCallback
import us.ltsoft.ventasapp.data.firestore.FirestoreService
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.response.settings.UsuarioResponse
import us.ltsoft.ventasapp.databinding.ActivitySignInBinding
import us.ltsoft.ventasapp.ui.activities.download.DownloadActivity
import us.ltsoft.ventasapp.utils.SharedPref
import us.ltsoft.ventasapp.utils.Utils


class SignInActivity : AppCompatActivity() {

    private val TAG = "SignInActivity"
    private lateinit var binding: ActivitySignInBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var firestoreService: FirestoreService
    private lateinit var sharedPref: SharedPref
    private lateinit var db: VentasDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firestoreService = FirestoreService(FirebaseFirestore.getInstance())

        sharedPref = SharedPref.getInstance()
        db = VentasDB.getInstance()

        val gso: GoogleSignInOptions = GoogleSignInOptions
            .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        val mGoogleSignInClient: GoogleSignInClient = GoogleSignIn.getClient(this, gso)

        auth = FirebaseAuth.getInstance()

        binding.continueButton.setOnClickListener {
            sharedPref.isLoggedIn = true
            if (db.getCategoriasDao().getAllCategorias().isEmpty()) {
                val intent = Intent(this, DownloadActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        binding.signInButton.setOnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            resultLauncher.launch(signInIntent)
        }

    }

    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                sharedPref.isLoggedIn = true
                val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    sharedPref.username = account?.displayName
                    sharedPref.userId = account?.email
                    locateUser(account!!)
                } catch (e: ApiException) {
                    binding.signInButton.visibility = View.VISIBLE
                    binding.continueButton.visibility = View.GONE
                }
            }
        }

    override fun onStart() {
        super.onStart()
        val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(this)
        if (account != null) {
            locateUser(account)
        } else {
            updateUI(false)
        }

    }

    private fun locateUser(account: GoogleSignInAccount) {
        firestoreService.findUserByEmail(account.email.toString(), object :
            FirestoreCallback<UsuarioResponse> {
            override fun onSuccess(result: UsuarioResponse?) {
                if (result != null) {
                    firestoreService.setUserLastLogin(account.email.toString())
                    sharedPref.storename = result.negocio
                    updateUI(true, account)
                } else {
                    sharedPref.storename = ""
                    updateUI(false)
                }
            }

            override fun onFailed(exception: Exception) {
                sharedPref.isLoggedIn = false
                sharedPref.username = ""
                sharedPref.userId = ""
            }
        })
    }

    private fun updateUI(toContinue: Boolean, account: GoogleSignInAccount? = null) {
        binding.signInButton.visibility = if (toContinue) View.GONE else View.VISIBLE
        binding.continueButton.text = account?.displayName
        binding.continueButton.visibility = if (toContinue) View.VISIBLE else View.INVISIBLE
        binding.continueButton.isEnabled = (sharedPref.storename!!.isNotEmpty())

    }
}


