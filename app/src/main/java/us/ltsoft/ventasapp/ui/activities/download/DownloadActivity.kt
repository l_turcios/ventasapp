package us.ltsoft.ventasapp.ui.activities.download

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.firestore.FirestoreService
import us.ltsoft.ventasapp.data.model.db.PersistData
import us.ltsoft.ventasapp.data.response.CategoriaResponse
import us.ltsoft.ventasapp.data.response.ClienteResponse
import us.ltsoft.ventasapp.data.response.EscalaResponse
import us.ltsoft.ventasapp.data.response.ProductoPrecioResponse
import us.ltsoft.ventasapp.data.response.ProductoResponse
import us.ltsoft.ventasapp.ui.activities.main.MainActivity
import us.ltsoft.ventasapp.utils.SharedPref

class DownloadActivity : AppCompatActivity() {

    private val TAG = "DownloadActivity"
    private lateinit var progressBar: ProgressBar

    private lateinit var firestoreService: FirestoreService
    private lateinit var persistData: PersistData
    private lateinit var sharedPref: SharedPref

    private var clients: List<ClienteResponse>? = null
    private var categories: List<CategoriaResponse>? = null
    private var scales: List<EscalaResponse>? = null
    private var products: List<ProductoResponse>? = null
    private var productsPrices: List<ProductoPrecioResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_download)

        progressBar = findViewById(R.id.progressBar)
        persistData = PersistData()
        firestoreService = FirestoreService(FirebaseFirestore.getInstance(), persistData)
        sharedPref = SharedPref.getInstance()

        Log.d(TAG, "onCreate: ")
        performFirestoreRequests()
    }

    private fun showProgress() {
        progressBar.visibility = ProgressBar.VISIBLE
    }

    private fun hideProgress() {
        progressBar.visibility = ProgressBar.GONE
    }

    private fun showToast(message: String) {
        Log.d(TAG, "showToast: $message")
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun handleFirestoreRequestError(error: Throwable) {
        showToast("Error en la solicitud: ${error.message}")
    }

    private fun handleFirestoreRequestsSuccess() {
        persistData.persistClientes(clients!!)
        persistData.persistCategorias(categories!!)
        persistData.persistEscalas(scales!!)
        persistData.persistProductos(products!!)
        persistData.persistProductoPrecios(productsPrices!!)

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun performFirestoreRequests() {
        showProgress()

        val coroutineScope = CoroutineScope(Job() + Dispatchers.Main)

        coroutineScope.launch {
            try {
                categories = firestoreService.getCollection(SharedPref.CATEGORIES_COLLECTION)
                scales = firestoreService.getCollection(SharedPref.SCALES_COLLECTION)
                products = firestoreService.getCollection(SharedPref.PRODUCTS_COLLECTION)
                productsPrices = firestoreService.getCollection(SharedPref.PRODUCTPRICES_COLLECTION)
                clients = firestoreService.getCollection(SharedPref.CLIENTS_COLLECTION)
                firestoreService.getSalesWithDetails(sharedPref.userId.toString())
                handleFirestoreRequestsSuccess()
            } catch (e: Exception) {
                handleFirestoreRequestError(e)
            } finally {
                hideProgress()
            }
        }
    }

}