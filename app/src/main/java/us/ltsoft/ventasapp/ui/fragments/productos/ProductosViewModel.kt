package us.ltsoft.ventasapp.ui.fragments.productos

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.dto.ProductoDTO

class ProductosViewModel : ViewModel() {
    private val db = VentasDB.getInstance()
    var productos: LiveData<List<ProductoDTO>> = db.getProductosDao().getAllProductosDTO()

    fun itemCount(): Int {
        return productos.value?.size ?: 0
    }
}