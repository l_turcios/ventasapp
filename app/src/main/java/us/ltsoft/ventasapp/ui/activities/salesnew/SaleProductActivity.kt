package us.ltsoft.ventasapp.ui.activities.salesnew

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextWatcher
import androidx.recyclerview.widget.LinearLayoutManager
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.adapters.OnProductoClickListener
import us.ltsoft.ventasapp.adapters.ProductosNuevaVentaAdapter
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.ProductoPrecio
import us.ltsoft.ventasapp.data.model.entities.VentasDetalle
import us.ltsoft.ventasapp.data.model.entities.dto.ProductoDTO
import us.ltsoft.ventasapp.databinding.ActivitySaleProductBinding
import us.ltsoft.ventasapp.utils.SharedPref
import us.ltsoft.ventasapp.utils.Utils

class SaleProductActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySaleProductBinding
    private lateinit var adapter: ProductosNuevaVentaAdapter
    private lateinit var db: VentasDB
    private var productoPrecio: ProductoPrecio? = null
    private var ventaDetalle: VentasDetalle? = null
    private lateinit var sharedPref: SharedPref
    private var producto: String? = null
    private lateinit var escalaId: String
    private lateinit var ventaId: String
    private var cantidad: Int = 0
    private var precio: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySaleProductBinding.inflate(layoutInflater)
        setContentView(binding.root)
        db = VentasDB.getInstance()
        sharedPref = SharedPref.getInstance()
        adapter = ProductosNuevaVentaAdapter(db.getProductosDao().getProductosWithCategoriaDTO())

        if(intent.hasExtra("ventaId") && intent.hasExtra("escalaId")) {
            ventaId = intent.getStringExtra("ventaId")!!
            escalaId = intent.getStringExtra("escalaId")!!
        }

        adapter.setOnProductoClickListener(object: OnProductoClickListener {
            override fun onProductoClick(producto: ProductoDTO) {
                binding.btnContinuarNuevaVentaConProducto.isEnabled = false
                this@SaleProductActivity.producto = producto.id
                if(this@SaleProductActivity.producto!!.isNotEmpty() &&
                    binding.editTextCantidadProductoNuevaVenta.text.toString().isNotEmpty()) {
                    cantidad = binding.editTextCantidadProductoNuevaVenta.text.toString().toInt()
                    productoPrecio = db.getProductoPreciosDao()
                        .getProductoPrecioByProductoIdAndEscalaIdAndCantidadTest(producto.id, escalaId, cantidad)
                    if(productoPrecio != null) {
                        precio = productoPrecio!!.precio
                        binding.textValorTotalProductoNuevaVenta.text = String.format("%.2f", precio * cantidad)
                        binding.btnContinuarNuevaVentaConProducto.isEnabled = true
                    } else {
                        binding.textValorTotalProductoNuevaVenta.text = getString(R.string.money_placeholder)
                    }
                }
            }
        })

        val recyclerView = binding.rvProductosNuevaVenta
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)

        val editTextBusucarProducto = binding.editTextBuscarProductoNuevaVenta
        editTextBusucarProducto.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                producto = null
                adapter.filter.filter(s)
            }

            override fun afterTextChanged(s: android.text.Editable?) {

            }
        })

        val editTextCantidadProducto = binding.editTextCantidadProductoNuevaVenta
        editTextCantidadProducto.setText("1")
        editTextCantidadProducto.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                cantidad = 0
                binding.textValorTotalProductoNuevaVenta.text = getString(R.string.money_placeholder)
                binding.btnContinuarNuevaVentaConProducto.isEnabled = false
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: android.text.Editable?) {
                if(s.toString().isNotEmpty()) {
                    cantidad = s.toString().toInt()
                    if(producto != null) {
                        productoPrecio = db.getProductoPreciosDao()
                            .getProductoPrecioByProductoIdAndEscalaIdAndCantidadTest(producto!!, escalaId, cantidad)
                        if(productoPrecio != null) {
                            precio = productoPrecio!!.precio
                            binding.textValorTotalProductoNuevaVenta.text = String.format("$ %.2f", precio * cantidad)
                            binding.btnContinuarNuevaVentaConProducto.isEnabled = true
                        } else {
                            binding.textValorTotalProductoNuevaVenta.text = getString(R.string.money_placeholder)
                        }
                    }
                } else {
                    cantidad = 0
                    binding.textValorTotalProductoNuevaVenta.text = getString(R.string.money_placeholder)
                }
            }
        })

        binding.btnContinuarNuevaVentaConProducto.setOnClickListener {
            if(producto != null && binding.editTextCantidadProductoNuevaVenta.text.toString().toInt() > 0) {
                ventaDetalle = db.getVentasDetalleDao().getVentasDetalleByVentaIdAndProductoId(ventaId, producto!!)
                if(ventaDetalle != null){
                    cantidad += ventaDetalle!!.cantidad
                    productoPrecio = db.getProductoPreciosDao()
                        .getProductoPrecioByProductoIdAndEscalaIdAndCantidadTest(producto!!, escalaId, cantidad)
                    precio = if(productoPrecio != null) {
                        productoPrecio!!.precio
                    } else {
                        0.00
                    }
                    ventaDetalle!!.cantidad = cantidad
                    ventaDetalle!!.precio = precio
                    ventaDetalle!!.total = precio * cantidad
                    db.getVentasDetalleDao().upsertVentasDetalle(ventaDetalle!!)
                    sharedPref.isThereNewSale = true
                    db.getVentasDao().updateTotalVenta(ventaId, db.getVentasDetalleDao().getTotalVenta(ventaId))
                    finish()
                    return@setOnClickListener
                } else {
                    ventaDetalle =
                        VentasDetalle(
                            Utils().generarCodigo(), ventaId, producto!!, cantidad, precio,precio * cantidad,false
                        )
                    db.getVentasDetalleDao().upsertVentasDetalle(ventaDetalle!!)
                    sharedPref.isThereNewSale = true
                }
                db.getVentasDao().updateTotalVenta(ventaId, db.getVentasDetalleDao().getTotalVenta(ventaId))
                finish()
            }
        }



    }
}