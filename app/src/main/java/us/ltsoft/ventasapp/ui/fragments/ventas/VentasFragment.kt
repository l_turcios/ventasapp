package us.ltsoft.ventasapp.ui.fragments.ventas

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import us.ltsoft.ventasapp.adapters.VentasAdapter
import us.ltsoft.ventasapp.databinding.FragmentVentasBinding
import us.ltsoft.ventasapp.ui.activities.main.MainActivity
import us.ltsoft.ventasapp.utils.printer.BluetoothCheck
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class VentasFragment : Fragment() {

    private var _binding: FragmentVentasBinding? = null
    private val binding get() = _binding!!
    private lateinit var ventasViewModel: VentasViewModel
    private val sharedPref = us.ltsoft.ventasapp.utils.SharedPref.getInstance()
    private var device: BluetoothDevice? = null
    private var now = Calendar.getInstance()
    private var fecha = ""
    private var total = 0.0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentVentasBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ventasViewModel = ViewModelProvider(this)[VentasViewModel::class.java]

        val recyclerView = binding.rvVentas
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = null //VentasAdapter(ventasViewModel.ventas)

        binding.btnImprimir.setOnClickListener {
            if(total == 0.0){
                return@setOnClickListener
            }
            val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
            if (bluetoothAdapter == null) {
                Toast.makeText(
                    requireContext(),
                    "Bluetooth no soportado",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            device = BluetoothCheck(bluetoothAdapter, sharedPref, device, this@VentasFragment, requireContext()).eval()
            if(device == null){
                return@setOnClickListener
            }

            val sweetAlertDialog = SweetAlertDialog(requireContext(), SweetAlertDialog.WARNING_TYPE)
            sweetAlertDialog.titleText = "Imprimir"
            sweetAlertDialog.contentText = "¿Desea imprimir el historial de ventas?"
            sweetAlertDialog.confirmText = "Imprimir"
            sweetAlertDialog.cancelText = "Cancelar"
            sweetAlertDialog.setConfirmClickListener {
                sweetAlertDialog.dismissWithAnimation()
                if(ventasViewModel.imprimirHistorial(device!!)){
                    it.changeAlertType(SweetAlertDialog.SUCCESS_TYPE)
                    it.titleText = "Impresión exitosa"
                    it.contentText = "El reporte de ventas se imprimió correctamente"
                    it.confirmText = "Aceptar"
                    it.setConfirmClickListener {
                        it.dismissWithAnimation()
                    }
                }else{
                    it.changeAlertType(SweetAlertDialog.ERROR_TYPE)
                    it.titleText = "Error de impresión"
                    it.contentText = "Ocurrió un error al imprimir el reporte de ventas"
                    it.confirmText = "Aceptar"
                    it.setConfirmClickListener {
                        it.dismissWithAnimation()
                    }
                }
            }
            sweetAlertDialog.setCancelClickListener {
                sweetAlertDialog.dismissWithAnimation()
            }
            sweetAlertDialog.show()
        }

        binding.btnFiltrarFecha.setOnClickListener {
            val datePickerDialog = DatePickerDialog.newInstance { _, year, monthOfYear, dayOfMonth ->
                fecha = "$year-${monthOfYear + 1}-$dayOfMonth"
                val dateValue = SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(fecha)
                ventasViewModel.filtrarVentasPorFecha(fecha).apply {
                    total = 0.0
                    recyclerView.adapter = VentasAdapter(ventasViewModel.ventas)
                    ventasViewModel.ventas.forEach {
                        total += it.total
                    }
                    updateTotal()
                }
                binding.btnFiltrarFecha.text =
                    String.format(SimpleDateFormat("dd/MM", Locale.US).format(dateValue!!))
            }

            datePickerDialog.version = DatePickerDialog.Version.VERSION_2
            datePickerDialog.setAccentColor("#1976D2")
            datePickerDialog.setOkColor("#1976D2")
            datePickerDialog.setCancelColor("#FF76D2")
            datePickerDialog.setOkText("Aceptar")
            datePickerDialog.setCancelText("Cancelar")
            datePickerDialog.locale = requireContext().resources.configuration.locale
            datePickerDialog.maxDate = now
            datePickerDialog.show(requireActivity().supportFragmentManager, "VentasDatePickerDialog")

        }

    }

    private fun updateTotal() {
        binding.tvTotalVentas.visibility = if(total == 0.0) View.GONE else View.VISIBLE
        binding.tvTotalVentas.text = String.format("$%.2f", total)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)!!.setFabVisibility(false)
    }

}