package us.ltsoft.ventasapp.ui.fragments.clientes

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO

class ClientesViewModel : ViewModel() {
    private val db = VentasDB.getInstance()
    var clientes: LiveData<List<ClienteDTO>> = db.getClientesDao().getAllClientesDTO()

    fun itemCount(): Int {
        return clientes.value?.size ?: 0
    }
}