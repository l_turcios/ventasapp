package us.ltsoft.ventasapp.ui.fragments.clientes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.firestore.FirebaseFirestore
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.firestore.FirestoreService
import us.ltsoft.ventasapp.data.model.db.VentasDB
import us.ltsoft.ventasapp.data.model.entities.Cliente
import us.ltsoft.ventasapp.data.model.entities.Escala
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO
import us.ltsoft.ventasapp.data.response.ClienteResponse
import us.ltsoft.ventasapp.databinding.FragmentClienteBinding
import us.ltsoft.ventasapp.ui.activities.main.MainActivity
import us.ltsoft.ventasapp.utils.Utils

class ClienteFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentClienteBinding? = null
    private val binding get() = _binding!!
    private val db = VentasDB.getInstance()
    private val escalas = db.getEscalasDao().getAllEscalas()
    private var cliente: Cliente? = null
    private lateinit var escala: Escala
    private var clienteId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        clienteId = arguments?.getString("clienteId")
         _binding = FragmentClienteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkArguments()

        binding.btnCancelarClienteFragmentCliente.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.btnGuardarClienteFragmentCliente.setOnClickListener {
            val nombre = binding.editNombreClienteFragmentCliente.text.toString()
            val telefono = binding.editTelefonoClienteFragmentCliente.text.toString()
            val escalaId = escalas[binding.spinnerTipoClienteFragmentCliente.selectedItemPosition].id
            if (clienteId != null) {
                cliente?.id = clienteId!!
                cliente?.nombre = nombre
                cliente?.telefono = telefono
                cliente?.escalaId = escalaId
                cliente?.sincronizado = false
            } else {
                cliente = Cliente(Utils().generarCodigo(), nombre, telefono, escalaId, false)
            }
            db.getClientesDao().upsertCliente(cliente!!)
            val sweetAlertDialog : SweetAlertDialog = SweetAlertDialog(requireContext(), SweetAlertDialog.SUCCESS_TYPE)
            sweetAlertDialog.titleText = "Cliente guardado"
            sweetAlertDialog.contentText = "El cliente se ha guardado correctamente"
            sweetAlertDialog.setConfirmClickListener {
                setFragmentResult("clienteId",
                    bundleOf("clienteId" to cliente!!.id,
                        "nombre" to cliente!!.nombre,
                        "telefono" to cliente!!.telefono,
                        "escalaId" to cliente!!.escalaId
                    ))
                it.dismissWithAnimation()
                requireActivity().onBackPressed()
            }
            sweetAlertDialog.show()
        }
    }

    private fun checkArguments() {
        if (clienteId != null) {
            cliente = db.getClientesDao().getClienteById(clienteId!!)
            if (cliente != null && cliente!!.escalaId.isNotEmpty()) {
                escala = db.getEscalasDao().getEscalaById(cliente!!.escalaId)
            }
        }
        updateView()
    }

    private fun updateView() {
        (activity as? MainActivity?)!!.supportActionBar?.title = if(clienteId != null) "Editar cliente" else "Nuevo cliente"
        binding.editNombreClienteFragmentCliente.setText(cliente?.nombre ?: "")
        binding.editTelefonoClienteFragmentCliente.setText(cliente?.telefono ?: "")
        binding.spinnerTipoClienteFragmentCliente.adapter = android.widget.ArrayAdapter(
            requireContext(),
            R.layout.item_spinner_list,
            escalas.map { it.descripcion }
        )
        escalas.forEach {
            if (it.id == cliente?.escalaId) {
                binding.spinnerTipoClienteFragmentCliente.setSelection(escalas.indexOf(it))
            }
        }

    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity?)!!.setFabVisibility(false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}