package us.ltsoft.ventasapp.utils.printer

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import us.ltsoft.ventasapp.utils.SharedPref

class BluetoothCheck(val bluetoothAdapter: BluetoothAdapter,
                     val sharedPref: SharedPref,
                     var device: BluetoothDevice?,
                     var fragment: Fragment,
                     val context: Context
                     ) {
    companion object {
        private const val REQUEST_ENABLE_BT = 1
    }

//    var isFoundPrinter = false
    fun eval(): BluetoothDevice? {
        if (!bluetoothAdapter.isEnabled) {
            fragment.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode != Activity.RESULT_OK) {
                    Toast.makeText(
                        context,
                        "Bluetooth no habilitado",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@registerForActivityResult
                }
                if (ActivityCompat.checkSelfPermission(
                        context,
                        Manifest.permission.BLUETOOTH_CONNECT
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        ActivityCompat.requestPermissions(
                            fragment.requireActivity(),
                            arrayOf(Manifest.permission.BLUETOOTH_CONNECT),
                            REQUEST_ENABLE_BT
                        )
                    }
                } else {
                    val pairedDevices: Set<BluetoothDevice>? =
                        bluetoothAdapter.bondedDevices
                    pairedDevices?.forEach { device ->
                        if (device.name == sharedPref.printerName) {
                            this.device = device
                            return@forEach
                        }
                    }
                    if (device == null) {
                        Toast.makeText(
                            context,
                            "No se encontró la impresora",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.BLUETOOTH_CONNECT
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                ActivityCompat.requestPermissions(
                    fragment.requireActivity(),
                    arrayOf(Manifest.permission.BLUETOOTH_CONNECT),
                    REQUEST_ENABLE_BT
                )
            }
        } else {
            val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter.bondedDevices
            pairedDevices?.forEach { device ->
                if (device.name == sharedPref.printerName) {
                    this.device = device
                    return@forEach
                }
            }
            if (device == null) {
                Toast.makeText(
                    context,
                    "No se encontró la impresora",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        return this.device
    }
}