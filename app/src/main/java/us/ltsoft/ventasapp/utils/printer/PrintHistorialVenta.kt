package us.ltsoft.ventasapp.utils.printer

import android.annotation.SuppressLint
import android.bluetooth.BluetoothSocket
import us.ltsoft.ventasapp.data.model.entities.dto.VentaDTO
import java.io.IOException

class PrintHistorialVenta(
    private val socket: BluetoothSocket,
    private val ventas: List<VentaDTO>,
    private val onPrintingCompleteListener: OnPrintingCompleteListener
) : Thread() {

    interface OnPrintingCompleteListener {
        fun onPrintingComplete()
        fun onPrintingFailed(error: String)
    }

    private val sharedPref = us.ltsoft.ventasapp.utils.SharedPref.getInstance()

    @SuppressLint("MissingPermission")
    override fun run() {
        try {
            var total = 0.0
            socket.connect()
            val outputStream = socket.outputStream
            PrintVentaHeader(outputStream).printHeader(sharedPref.storename!!, sharedPref.username!!)

            outputStream.write(PrintConstants.ALIGN_LEFT)
            outputStream.write(PrintConstants.FONT_BOLD)
            ventas.forEach {
                total += it.total
                val clienteStr = it.cliente?.uppercase()?.trim() ?: "SIN NOMBRE"
                outputStream.write("${clienteStr}\n".toByteArray())
                val str = String.format("%-18s %s %6.2f", it.fechaString!!.trim(), "   $", it.total)
                outputStream.write(str.toByteArray())
                outputStream.write(PrintConstants.LINE_FEED)
            }
            outputStream.write(PrintConstants.ALIGN_CENTER)
            outputStream.write(PrintConstants.LINE)

            outputStream.write(PrintConstants.ALIGN_LEFT)
            outputStream.write(PrintConstants.FONT_HEIGHT_WIDTH)
            val str = String.format("%8s %.2f", "TOTAL $", total)
            outputStream.write(str.toByteArray())
            outputStream.write(PrintConstants.LINE_FEED)
            outputStream.write(PrintConstants.LINE_FEED)
            outputStream.write(PrintConstants.FEED_PAPER_AND_CUT)
            outputStream.flush()
            outputStream.close()
            onPrintingCompleteListener.onPrintingComplete()
        } catch (e: IOException) {
            e.printStackTrace()
            onPrintingCompleteListener
                .onPrintingFailed("Error de conexión o escritura en el outputStream")
        } catch (e: Exception) {
            e.printStackTrace()
            onPrintingCompleteListener
                .onPrintingFailed("Error general durante la impresión")
        } finally {
            try {
                socket.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}