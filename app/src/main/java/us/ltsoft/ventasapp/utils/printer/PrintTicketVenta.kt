package us.ltsoft.ventasapp.utils.printer

import android.annotation.SuppressLint
import android.bluetooth.BluetoothSocket
import androidx.lifecycle.LiveData
import us.ltsoft.ventasapp.data.model.entities.dto.VentaDTO
import us.ltsoft.ventasapp.data.model.entities.dto.VentasDetalleDTO
import java.io.IOException

class PrintTicketVenta(
    val socket: BluetoothSocket,
    val venta: VentaDTO,
    val ventaDetalle: LiveData<List<VentasDetalleDTO>>,
    val onPrintingCompleteListener: OnPrintingCompleteListener
) : Thread() {

    interface OnPrintingCompleteListener {
        fun onPrintingComplete()
        fun onPrintingFailed(error: String)
    }

    val sharedPref = us.ltsoft.ventasapp.utils.SharedPref.getInstance()
    @SuppressLint("MissingPermission")
    override fun run() {
        try {
            socket.connect()
            val outputStream = socket.outputStream
            PrintVentaHeader(outputStream).printHeader(sharedPref.storename!!, sharedPref.username!!)
            outputStream.write("Cliente:\n".toByteArray())
            outputStream.write(PrintConstants.FONT_BOLD)
            outputStream.write(venta.cliente!!.trim().plus("\n").toByteArray())
            outputStream.write(PrintConstants.FONT_NORMAL)
            outputStream.write(PrintConstants.LINE)
            outputStream.write(PrintConstants.ALIGN_LEFT)
            outputStream.write(PrintConstants.FONT_BOLD)
            ventaDetalle.value?.forEach {
                outputStream.write("${it.producto.uppercase().trim()}\n".toByteArray())
                val str = String.format(PrintConstants.ITEM_VENTA_DETAIL,
                    "Cant", it.cantidad, "$", it.precio, " ", "$", it.total)
                outputStream.write(str.toByteArray())
                outputStream.write(PrintConstants.LINE_FEED)
            }
            outputStream.write(PrintConstants.ALIGN_CENTER)
            outputStream.write(PrintConstants.LINE)

            outputStream.write(PrintConstants.ALIGN_LEFT)
            outputStream.write(PrintConstants.FONT_HEIGHT_WIDTH)
            val str = String.format("%8s %.2f", "TOTAL $", venta.total)
            outputStream.write(str.toByteArray())
            outputStream.write(PrintConstants.LINE_FEED)
            outputStream.write(PrintConstants.ALIGN_CENTER)
            outputStream.write(PrintConstants.FONT_NORMAL)
            outputStream.write(PrintConstants.FONT_BOLD)
            val footer = String.format("%s\n", venta.fechaString.let { it?.trim() ?: "" })
            outputStream.write(footer.toByteArray())
            outputStream.write("Gracias por su compra\n".toByteArray())
            outputStream.write(PrintConstants.LINE_FEED)
            outputStream.write(PrintConstants.LINE_FEED)
            outputStream.write(PrintConstants.FEED_PAPER_AND_CUT)
            outputStream.flush()
            outputStream.close()
            onPrintingCompleteListener.onPrintingComplete()
        } catch (e: IOException) {
            // Manejar las excepciones relacionadas con la conexión o escritura en el outputStream
            e.printStackTrace()
            onPrintingCompleteListener
                .onPrintingFailed("Error de conexión o escritura en el outputStream")
        } catch (e: Exception) {
            // Manejar otras excepciones generales
            e.printStackTrace()
            onPrintingCompleteListener
                .onPrintingFailed("Error general durante la impresión")
        } finally {
            try {
                socket.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}