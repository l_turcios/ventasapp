package us.ltsoft.ventasapp.utils

open class Utils {

    fun generarCodigo(): String {

        val codigo = StringBuilder()
        for (i in 1..20) {

            when (val numeroAleatorio = (0..61).random()) {
                // digitos
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9 -> codigo.append(numeroAleatorio)

                // abecedario
                10 -> codigo.append('a')
                11 -> codigo.append('A')

                12 -> codigo.append('b')
                13 -> codigo.append('B')

                14 -> codigo.append('c')
                15 -> codigo.append('C')

                16 -> codigo.append('d')
                17 -> codigo.append('D')

                18 -> codigo.append('e')
                19 -> codigo.append('E')

                20 -> codigo.append('f')
                21 -> codigo.append('F')

                22 -> codigo.append('g')
                23 -> codigo.append('G')

                24 -> codigo.append('h')
                25 -> codigo.append('H')

                26 -> codigo.append('i')
                27 -> codigo.append('I')

                28 -> codigo.append('j')
                29 -> codigo.append('J')

                30 -> codigo.append('k')
                31 -> codigo.append('K')

                // en lugar de la "l" colocar un 1 para evitar confusion
                32 -> codigo.append('1')
                33 -> codigo.append('L')

                34 -> codigo.append('m')
                35 -> codigo.append('M')

                36 -> codigo.append('n')
                37 -> codigo.append('N')

                // En lugar de poner la "o" mejor poner cero para evitar confusion
                38 -> codigo.append('0')
                39 -> codigo.append('0')

                40 -> codigo.append('p')
                41 -> codigo.append('P')

                42 -> codigo.append('q')
                43 -> codigo.append('Q')

                44 -> codigo.append('r')
                45 -> codigo.append('R')

                46 -> codigo.append('s')
                47 -> codigo.append('S')

                48 -> codigo.append('t')
                49 -> codigo.append('T')

                50 -> codigo.append('u')
                51 -> codigo.append('U')

                52 -> codigo.append('v')
                53 -> codigo.append('V')

                54 -> codigo.append('w')
                55 -> codigo.append('W')

                56 -> codigo.append('x')
                57 -> codigo.append('X')

                58 -> codigo.append('y')
                59 -> codigo.append('Y')

                60 -> codigo.append('z')
                61 -> codigo.append('Z')
            }
        }

        return codigo.toString()
    }
}