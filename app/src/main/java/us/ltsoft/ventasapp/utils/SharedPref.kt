package us.ltsoft.ventasapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class SharedPref {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var context: Context
        private const val PREFS_NAME = "MyAppPreferences"
        private const val KEY_USERNAME = "username"
        private const val KEY_USER_ID = "user_id"
        private const val KEY_IS_LOGGED_IN = "is_logged_in"
        private const val KEY_STORE_NAME = "store_name"
        private const val KEY_SALES_ID = "sales_id"

        const val CATEGORIES_COLLECTION = "categorias"
        const val CLIENTS_COLLECTION = "clientes"
        const val SCALES_COLLECTION = "escalas"
        const val PRODUCTS_COLLECTION = "productos"
        const val PRODUCTPRICES_COLLECTION = "productosPrecios"
        const val SALES_COLLECTION = "ventas"
        const val SALESDETAILS_COLLECTION = "ventasDetalle"
        const val USERS_COLLECTION = "usuarios"

        const val NEW_SALE_FOUND = "new_sale_found"

        const val PRINTER_NAME = "printer_name"
        fun initialize(context: Context){
            this.context = context.applicationContext
        }

        fun getInstance(): SharedPref {
            return SharedPref()
        }

    }

    var username: String?
        get() = sharedPreferences.getString(KEY_USERNAME, "")
        set(value) = sharedPreferences.edit().putString(KEY_USERNAME, value).apply()

    var storename: String?
        get() = sharedPreferences.getString(KEY_STORE_NAME, "")
        set(value) = sharedPreferences.edit().putString(KEY_STORE_NAME, value).apply()

    var userId: String?
        get() = sharedPreferences.getString(KEY_USER_ID, "")
        set(value) = sharedPreferences.edit().putString(KEY_USER_ID, value).apply()

    var isLoggedIn: Boolean
        get() = sharedPreferences.getBoolean(KEY_IS_LOGGED_IN, false)
        set(value) = sharedPreferences.edit().putBoolean(KEY_IS_LOGGED_IN, value).apply()

    var salesId: String?
        get() = sharedPreferences.getString(KEY_SALES_ID, "")
        set(value) = sharedPreferences.edit().putString(KEY_SALES_ID, value).apply()

    var isThereNewSale: Boolean
        get() = sharedPreferences.getBoolean(NEW_SALE_FOUND, false)
        set(value) = sharedPreferences.edit().putBoolean(NEW_SALE_FOUND, value).apply()

    var printerName: String
        get() = sharedPreferences.getString(PRINTER_NAME, "BlueTooth Printer") ?: "BlueTooth Printer"
        set(value) = sharedPreferences.edit().putString(PRINTER_NAME, value).apply()

    fun clear() {
        sharedPreferences.edit().clear().apply()
    }

}