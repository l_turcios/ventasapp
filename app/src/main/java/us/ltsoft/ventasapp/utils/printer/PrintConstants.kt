package us.ltsoft.ventasapp.utils.printer

import java.util.UUID

class PrintConstants {
    companion object {
        val MY_UUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        val RESET = byteArrayOf(0x1B, 0x40)
        val FONT_NORMAL = byteArrayOf(0x1B, 0x21, 0x00)
        val FONT_BOLD = byteArrayOf(0x1B, 0x21, 0x08)
        val FONT_HEIGHT = byteArrayOf(0x1B, 0x21, 0x10)
        val FONT_WIDTH = byteArrayOf(0x1B, 0x21, 0x20)
        val FONT_HEIGHT_WIDTH = byteArrayOf(0x1B, 0x21, 0x30)
        val ALIGN_LEFT = byteArrayOf(0x1B, 0x61, 0x00)
        val ALIGN_CENTER = byteArrayOf(0x1B, 0x61, 0x01)
        val ALIGN_RIGHT = byteArrayOf(0x1B, 0x61, 0x02)
        val LINE_FEED = byteArrayOf(0x0A)
        val FEED_PAPER_AND_CUT = byteArrayOf(0x1D, 0x56, 0x00)
        val LINE = "--------------------------------\n".toByteArray()
        val ITEM_VENTA_DETAIL = "%-6s%3s  %s %5.2f%3s%1s%6.2f\n"
    }
}