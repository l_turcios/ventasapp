package us.ltsoft.ventasapp.utils.printer

import java.io.OutputStream

class PrintVentaHeader (val outputStream: OutputStream){
    fun printHeader(storeName: String, userName: String){
        outputStream.write(PrintConstants.RESET)
        outputStream.write(PrintConstants.ALIGN_CENTER)
        outputStream.write(PrintConstants.FONT_HEIGHT)
        outputStream.write(PrintConstants.FONT_BOLD)
        outputStream.write(storeName.trim().toByteArray())
        outputStream.write(PrintConstants.FONT_NORMAL)
        outputStream.write(PrintConstants.FONT_BOLD)
        outputStream.write(PrintConstants.LINE_FEED)
        outputStream.write(userName.trim().toByteArray())
        outputStream.write(PrintConstants.LINE_FEED)
        outputStream.write(PrintConstants.FONT_NORMAL)
        outputStream.write(PrintConstants.LINE)
    }

}