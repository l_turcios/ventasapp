package us.ltsoft.ventasapp.data.model.dao

import androidx.room.Dao
import us.ltsoft.ventasapp.data.model.entities.Escala

@Dao
interface IEscalasDao {

    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    fun upsertEscala(escala: Escala)

    @androidx.room.Query("select * from escalas")
    fun getAllEscalas(): List<Escala>

    @androidx.room.Query("delete from escalas")
    fun deleteAllEscalas()

    @androidx.room.Query("select * from escalas where id = :id")
    fun getEscalaById(id: String): Escala
}