package us.ltsoft.ventasapp.data.model.entities.dto

class PrecioDTO (
    val id: String,
    val escalaId: String,
    val escala: String,
    val productoId: String,
    val precio: Double,
    val cantMin: Int,
    val cantMax: Int
)