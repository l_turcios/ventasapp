package us.ltsoft.ventasapp.data.model.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import us.ltsoft.ventasapp.data.model.entities.VentasDetalle
import us.ltsoft.ventasapp.data.model.entities.dto.VentasDetalleDTO

@Dao
interface IVentasDetalle {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertVentasDetalle(ventasDetalle: VentasDetalle)

    @Query("select * from ventas_detalle")
    fun getAllVentasDetalle(): List<VentasDetalle>

    @Query("delete from ventas_detalle")
    fun deleteAllVentasDetalle()

    @Query("select * from ventas_detalle where id = :id")
    fun getDetalleById(id: String): VentasDetalle

    @Query("select * from ventas_detalle where ventaId = :id")
    fun getVentasDetalleByVentaId(id: String): List<VentasDetalle>

    @Query("select * from ventas_detalle where ventaId = :id and productoId = :productoId")
    fun getVentasDetalleByVentaIdAndProductoId(id: String, productoId: String): VentasDetalle?

    @Query("select vd.id, vd.cantidad, vd.precio, vd.total, p.producto, c.categoria " +
            "FROM ventas_detalle AS vd " +
            "LEFT JOIN productos AS p ON p.id = vd.productoId " +
            "LEFT JOIN categorias AS c ON c.id = p.categoriaId " +
            "WHERE vd.ventaId = (SELECT id FROM ventas WHERE estado = 'En proceso' ORDER BY fecha DESC LIMIT 1)")
    fun getVentasDetalleEnProcesoDTO(): LiveData<List<VentasDetalleDTO>>

    @Query("select sum(total) from ventas_detalle where ventaId = :id")
    fun getTotalVenta(id: String): Double

    @Query("delete from ventas_detalle where id = :id")
    fun eliminarItem(id: String)

    fun sincronizadaVentaDetalle(id: String): Boolean {
        val ventaDetalle = getDetalleById(id)
        ventaDetalle.sincronizado = true
        upsertVentasDetalle(ventaDetalle)
        return true
    }

}