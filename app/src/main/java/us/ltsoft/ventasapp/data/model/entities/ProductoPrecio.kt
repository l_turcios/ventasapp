package us.ltsoft.ventasapp.data.model.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "productos_precios")
class ProductoPrecio (
    @PrimaryKey(autoGenerate = false)
    var id: String = "",
    var productoId: String = "",
    var cantMin: Int = 0,
    var cantMax: Int = 0,
    var escalaId: String,
    var precio: Double = 0.0,
    var sincronizado: Boolean = false
){}