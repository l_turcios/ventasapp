package us.ltsoft.ventasapp.data.model.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categorias")
class Categoria(
    @PrimaryKey(autoGenerate = false)
    var id: String,
    var categoria: String,
    var sincronizado: Boolean = false
)