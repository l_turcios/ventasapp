package us.ltsoft.ventasapp.data.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import us.ltsoft.ventasapp.data.model.entities.Escala

@Parcelize
class EscalaResponse(
    var id: String = "",
    var descripcion: String = ""
): Parcelable {

    fun toEscala(): Escala {
        return Escala(
            id = id,
            descripcion = descripcion,
            sincronizado = true
        )
    }

}