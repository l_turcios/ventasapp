package us.ltsoft.ventasapp.data.model.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import us.ltsoft.ventasapp.data.model.entities.Producto
import us.ltsoft.ventasapp.data.model.entities.dto.ProductoDTO

@Dao
interface IProductoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertProducto(producto: Producto)

    @Query("select * from productos")
    fun getAllProductos(): List<Producto>

    @Query("delete from productos")
    fun deleteAllProductos()

    @Query("select * from productos where id= :codigo")
    fun getProducto(codigo: String): Producto

    @Query("select p.id, p.producto, c.categoria from productos AS p " +
            "LEFT JOIN categorias AS c ON c.id = p.categoriaId" +
            " ORDER BY p.producto ASC")
    fun getProductosWithCategoriaDTO(): List<ProductoDTO>

    fun getAllProductosDTO(): LiveData<List<ProductoDTO>>{
        return getProductosWithCategoriaDTO().let {
            val productosDTO = mutableListOf<ProductoDTO>()
            it.forEach { productoDTO ->
                productosDTO.add(productoDTO)
            }
            return@let productosDTO
        }.let {
            val productosDTO = MutableLiveData<List<ProductoDTO>>()
            productosDTO.value = it
            return@let productosDTO
        }
    }

}