package us.ltsoft.ventasapp.data.response

import android.os.Build
import android.os.Parcelable
import androidx.annotation.RequiresApi
import com.google.firebase.Timestamp
import kotlinx.parcelize.Parcelize
import us.ltsoft.ventasapp.data.model.entities.Venta

@Parcelize
class VentaResponse (
    var id: String = "",
    var clienteId: String = "",
    var fecha: Timestamp? = null,
    var total: Double = 0.0,
    var estado: String = "",
    var usuarioId: String = "",
): Parcelable {

    @RequiresApi(Build.VERSION_CODES.O)
    fun toVenta(): Venta {
        return Venta(
            id = id,
            clienteId = clienteId,
            fecha = if(fecha != null) {
                fecha!!.toDate().toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDateTime()
            } else null,
            total = total,
            estado = estado,
            usuarioId = usuarioId,
            sincronizado = true
        )
    }

}