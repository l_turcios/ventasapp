package us.ltsoft.ventasapp.data.response.settings

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.parcelize.Parcelize

@Parcelize
class UsuarioResponse(
    var email: String = "",
    var negocio: String = "",
    var lastLogin: Timestamp? = null,
): Parcelable