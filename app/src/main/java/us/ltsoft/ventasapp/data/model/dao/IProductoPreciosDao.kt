package us.ltsoft.ventasapp.data.model.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import us.ltsoft.ventasapp.data.model.entities.ProductoPrecio
import us.ltsoft.ventasapp.data.model.entities.dto.PrecioDTO

@Dao
interface IProductoPreciosDao {

    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    fun upsertProductoPrecios(productoPrecios: ProductoPrecio)

    @androidx.room.Query("select * from productos_precios")
    fun getAllProductoPrecios(): List<ProductoPrecio>

    @androidx.room.Query("delete from productos_precios")
    fun deleteAllProductoPrecios()

    @androidx.room.Query("select * from productos_precios where productoId = :id")
    fun getProductoPrecioByProductoId(id: String): ProductoPrecio

    @androidx.room.Query("select * from productos_precios where productoId = :id and escalaId = :escalaId and :cantidad between cantMin and cantMax")
    fun getProductoPrecioByProductoIdAndEscalaIdAndCantidadTest(id: String, escalaId: String, cantidad: Int): ProductoPrecio?

    @Query("select pr.id, pr.escalaId, e.descripcion AS escala, pr.productoId, pr.cantMin, pr.cantMax, pr.precio from productos_precios AS pr " +
            "LEFT JOIN escalas AS e ON e.id = pr.escalaId WHERE pr.productoId = :id ORDER BY pr.escalaId, pr.cantMin ASC")
    fun getProductoPreciosByProductoId(id: String): LiveData<List<PrecioDTO>>

}