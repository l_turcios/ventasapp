package us.ltsoft.ventasapp.data.model.entities

import android.os.Build
import android.os.Parcelable
import androidx.annotation.RequiresApi
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Date

@Entity(tableName = "ventas")
@Parcelize
class Venta (
    @PrimaryKey(autoGenerate = false)
    var id: String = "",
    var fecha: LocalDateTime? = null,
    var clienteId: String = "",
    var total: Double = 0.0,
    var estado: String = "",
    var usuarioId: String = "",
    var sincronizado: Boolean = false
): Parcelable {
    val fechaString: String?
        get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fecha!!.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"))
        } else null
}