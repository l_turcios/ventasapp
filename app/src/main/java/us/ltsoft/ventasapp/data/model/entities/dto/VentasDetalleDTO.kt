package us.ltsoft.ventasapp.data.model.entities.dto

class VentasDetalleDTO (
    var id: String,
    var cantidad: Int,
    var precio: Double,
    var total: Double,
    var producto: String,
    var categoria: String
)
