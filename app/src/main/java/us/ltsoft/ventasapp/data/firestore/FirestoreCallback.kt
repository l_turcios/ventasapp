package us.ltsoft.ventasapp.data.firestore

interface FirestoreCallback<T> {

    fun onSuccess(result: T?)

    fun onFailed(exception: Exception)
}