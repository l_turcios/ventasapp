package us.ltsoft.ventasapp.data.model.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import us.ltsoft.ventasapp.data.model.entities.Cliente
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO

@Dao
interface IClientesDao {

    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    fun upsertCliente(cliente: Cliente)

    @Query("update clientes set sincronizado = 1 where id = :id")
    fun updateClienteSincronizado(id: String)

    @Query("select * from clientes")
    fun getAllClientes(): List<Cliente>

    @Query("delete from clientes")
    fun deleteAllClientes()

    @Query("select * from clientes where id = :id")
    fun getClienteById(id: String): Cliente?

    @Query("select c.id, c.nombre, c.telefono, e.descripcion as tipo from clientes AS c LEFT JOIN escalas AS e ON c.escalaId = e.id")
    fun getAllClientesDTO(): LiveData<List<ClienteDTO>>

}