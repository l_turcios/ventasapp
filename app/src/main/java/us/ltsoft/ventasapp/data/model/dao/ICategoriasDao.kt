package us.ltsoft.ventasapp.data.model.dao

import androidx.room.Dao
import us.ltsoft.ventasapp.data.model.entities.Categoria

@Dao
interface ICategoriasDao {

    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    fun upsertCategoria(categoria: Categoria)

    @androidx.room.Query("select * from categorias")
    fun getAllCategorias(): List<Categoria>

    @androidx.room.Query("delete from categorias")
    fun deleteAllCategorias()

    @androidx.room.Query("select * from categorias where id = :id")
    fun getCategoriaById(id: String): Categoria
}