package us.ltsoft.ventasapp.data.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import us.ltsoft.ventasapp.data.model.entities.VentasDetalle

@Parcelize
class VentasDetalleResponse(
    var id: String = "",
    var ventaId: String = "",
    var productoId: String = "",
    var cantidad: Int = 0,
    var precio: Double = 0.0,
    var total: Double = 0.0,
): Parcelable {

    fun toVentasDetalle(): VentasDetalle {
        return VentasDetalle(
            id = id,
            ventaId = ventaId,
            productoId = productoId,
            cantidad = cantidad,
            precio = precio,
            total = total,
            sincronizado = true
        )
    }

}
