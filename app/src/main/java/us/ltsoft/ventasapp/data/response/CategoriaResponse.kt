package us.ltsoft.ventasapp.data.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import us.ltsoft.ventasapp.data.model.entities.Categoria

@Parcelize
class CategoriaResponse (
    var id: String = "",
    var categoria: String = ""
): Parcelable {

    fun toCategoria() = Categoria(
        id = id,
        categoria = categoria,
        sincronizado = true
    )

}