package us.ltsoft.ventasapp.data.model.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "escalas")
class Escala (
    @PrimaryKey(autoGenerate = false)
    var id: String,
    var descripcion: String,
    var sincronizado: Boolean = false
){

}