package us.ltsoft.ventasapp.data.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import us.ltsoft.ventasapp.data.model.entities.Cliente

@Parcelize
class ClienteResponse(
    var id: String = "",
    var nombre: String = "",
    var telefono: String? = null,
    var escalaId: String = ""
) : Parcelable{

    fun toCliente(): Cliente {
        return Cliente(
            id = id,
            nombre = nombre,
            telefono = telefono,
            escalaId = escalaId,
            sincronizado = true
        )
    }
}
