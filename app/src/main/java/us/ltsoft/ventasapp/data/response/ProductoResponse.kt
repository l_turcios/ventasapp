package us.ltsoft.ventasapp.data.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import us.ltsoft.ventasapp.data.model.entities.Producto

@Parcelize
class ProductoResponse(
    var id: String = "",
    var categoriaId: String = "",
    var producto: String = "",
    var habilitado: Boolean = true
): Parcelable {

    fun toProducto(): Producto {
        return Producto(
            id = id,
            categoriaId = categoriaId,
            producto = producto,
            habilitado = habilitado,
            sincronizado = true
        )
    }

}