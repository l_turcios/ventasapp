package us.ltsoft.ventasapp.data.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import us.ltsoft.ventasapp.data.model.entities.ProductoPrecio

@Parcelize
class ProductoPrecioResponse(
    var id: String = "",
    var productoId: String = "",
    var cantMin: Int = 0,
    var cantMax: Int = 0,
    var escalaId: String = "",
    var precio: Double = 0.0
): Parcelable {

    fun toProductoPrecio(): ProductoPrecio {
        return ProductoPrecio(
            id = id,
            productoId = productoId,
            cantMin = cantMin,
            cantMax = cantMax,
            escalaId = escalaId,
            precio = precio,
            sincronizado = true
        )
    }

}