package us.ltsoft.ventasapp.data.model.entities.dto

class ClienteDTO(
    val id: String,
    val nombre: String,
    val tipo: String,
    val telefono: String
)