package us.ltsoft.ventasapp.data.model.db

import android.os.Build
import androidx.annotation.RequiresApi
import us.ltsoft.ventasapp.data.model.entities.Categoria
import us.ltsoft.ventasapp.data.model.entities.Cliente
import us.ltsoft.ventasapp.data.model.entities.Escala
import us.ltsoft.ventasapp.data.model.entities.Producto
import us.ltsoft.ventasapp.data.model.entities.ProductoPrecio
import us.ltsoft.ventasapp.data.model.entities.Venta
import us.ltsoft.ventasapp.data.model.entities.VentasDetalle
import us.ltsoft.ventasapp.data.response.CategoriaResponse
import us.ltsoft.ventasapp.data.response.ClienteResponse
import us.ltsoft.ventasapp.data.response.EscalaResponse
import us.ltsoft.ventasapp.data.response.ProductoPrecioResponse
import us.ltsoft.ventasapp.data.response.ProductoResponse
import us.ltsoft.ventasapp.data.response.VentaResponse
import us.ltsoft.ventasapp.data.response.VentasDetalleResponse

class PersistData() {

    val db = VentasDB.getInstance()

    fun persistCategorias(categorias: List<CategoriaResponse>) {
        categorias.forEach {
            db.getCategoriasDao().upsertCategoria(it.toCategoria())
        }
    }

    fun persistCategoria(categoria: CategoriaResponse){
        db.getCategoriasDao().upsertCategoria(categoria.toCategoria())
    }

    fun persistCategoria(categoria: Categoria){
        db.getCategoriasDao().upsertCategoria(categoria)
    }

    fun persistClientes(clientes: List<ClienteResponse>){
        clientes.forEach {
            db.getClientesDao().upsertCliente( it.toCliente())
        }
    }

    fun persistCliente(cliente: ClienteResponse){
        db.getClientesDao().upsertCliente(cliente.toCliente())
    }

    fun persistCliente(cliente: Cliente){
        db.getClientesDao().upsertCliente(cliente)
    }

    fun persistEscalas(escalas: List<EscalaResponse>){
        escalas.forEach {
            db.getEscalasDao().upsertEscala(it.toEscala())
        }
    }

    fun persistEscala(escala: EscalaResponse){
        db.getEscalasDao().upsertEscala(escala.toEscala())
    }

    fun persistEscala(escala: Escala){
        db.getEscalasDao().upsertEscala(escala)
    }

    fun persistProductos(productos: List<ProductoResponse>){
        productos.forEach {
            db.getProductosDao().upsertProducto(it.toProducto())
        }
    }

    fun persistProducto(producto: ProductoResponse){
        db.getProductosDao().upsertProducto(producto.toProducto())
    }

    fun persistProducto(producto: Producto){
        db.getProductosDao().upsertProducto(producto)
    }

    fun persistProductoPrecios(productoPrecios: List<ProductoPrecioResponse>){
        productoPrecios.forEach {
            db.getProductoPreciosDao().upsertProductoPrecios(it.toProductoPrecio())
        }
    }

    fun persistProductoPrecio(productoPrecio: ProductoPrecioResponse){
        db.getProductoPreciosDao().upsertProductoPrecios(productoPrecio.toProductoPrecio())
    }

    fun persistProductoPrecio(productoPrecio: ProductoPrecio){
        db.getProductoPreciosDao().upsertProductoPrecios(productoPrecio)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun persistVentas(ventas: List<VentaResponse?>){
        ventas.forEach {
            db.getVentasDao().upsertVenta(it!!.toVenta())
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun persistVenta(venta: VentaResponse){
        db.getVentasDao().upsertVenta(venta.toVenta())
    }

    fun persistVenta(venta: Venta){
        db.getVentasDao().upsertVenta(venta)
    }

    fun persistVentaDetalles(ventaDetalles: List<VentasDetalleResponse?>){
        ventaDetalles.forEach {
            db.getVentasDetalleDao().upsertVentasDetalle(it!!.toVentasDetalle())
        }
    }

    fun persistVentaDetalle(ventaDetalle: VentasDetalleResponse?){
        db.getVentasDetalleDao().upsertVentasDetalle(ventaDetalle!!.toVentasDetalle())
    }

    fun persistVentaDetalle(ventaDetalle: VentasDetalle){
        db.getVentasDetalleDao().upsertVentasDetalle(ventaDetalle)
    }

}