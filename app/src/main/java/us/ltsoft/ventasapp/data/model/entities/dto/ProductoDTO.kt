package us.ltsoft.ventasapp.data.model.entities.dto

class ProductoDTO(
    val id: String,
    val producto: String,
    val categoria: String,
)