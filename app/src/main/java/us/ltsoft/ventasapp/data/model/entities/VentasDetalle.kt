package us.ltsoft.ventasapp.data.model.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ventas_detalle")
class VentasDetalle (
    @PrimaryKey(autoGenerate = false)
    var  id: String,
    var ventaId: String,
    var productoId: String,
    var cantidad: Int,
    var precio: Double,
    var total: Double,
    var sincronizado: Boolean = false
){

}