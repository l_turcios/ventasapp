package us.ltsoft.ventasapp.data.model.entities.dto

import android.os.Build
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.LocalDateTime

@Parcelize
class VentaDTO(
    val venta: String,
    val cliente: String?,
    val clienteId: String?,
    val escalaId: String?,
    val fecha: LocalDateTime? = null,
    val total: Double
): Parcelable {
    val fechaString: String?
        get() = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            fecha?.format(java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"))
        } else null
}