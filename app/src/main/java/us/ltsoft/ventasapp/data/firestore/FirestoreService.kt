package us.ltsoft.ventasapp.data.firestore

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import us.ltsoft.ventasapp.data.model.db.PersistData
import us.ltsoft.ventasapp.data.response.VentaResponse
import us.ltsoft.ventasapp.data.response.VentasDetalleResponse
import us.ltsoft.ventasapp.data.response.settings.UsuarioResponse
import java.util.Calendar
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

const val TAG = "FirestoreService"

const val USERS_COLLECTION = "usuarios"
const val SALES_COLLECTION = "ventas"
const val SALES_DETAIL_COLLECTION = "ventasDetalle"
const val EMAIL_FIELD = "email"
const val LAST_LOGIN_FIELD = "lastLogin"

class FirestoreService(val firestore: FirebaseFirestore, private val persistData: PersistData? = null) {

    private val currentTime: Timestamp
        get() = Timestamp.now()

    val fechaLimite = Calendar.getInstance().apply {
        add(Calendar.DAY_OF_YEAR, -7)
    }.time


    fun findUserByEmail(email: String, callback: FirestoreCallback<UsuarioResponse>) {
        firestore.collection(USERS_COLLECTION)
            .whereEqualTo(EMAIL_FIELD, email)
            .get()
            .addOnSuccessListener { result ->
                if (result.isEmpty) {
                    callback.onSuccess(null)
                } else {
                    val user = result.documents[0].toObject(UsuarioResponse::class.java)
                    callback.onSuccess(user)
                }
            }
            .addOnFailureListener { exception ->
                callback.onFailed(exception)
            }
    }

    fun setUserLastLogin(email: String) {
        firestore.collection(USERS_COLLECTION)
            .whereEqualTo(EMAIL_FIELD, email)
            .get()
            .addOnSuccessListener { result ->
                if (result.isEmpty) {
                    val user = UsuarioResponse(email = email, lastLogin = currentTime, negocio = "")
                    firestore.collection(USERS_COLLECTION)
                        .add(user)
                } else {
                    val user = UsuarioResponse(lastLogin = currentTime)
                    firestore.collection(USERS_COLLECTION)
                        .document(result.documents[0].id)
                        .update(LAST_LOGIN_FIELD, user.lastLogin)
                }
            }
            .addOnFailureListener { exception ->
                // Do nothing
            }
    }

    suspend inline fun <reified T> getCollection(collection: String): List<T> = withContext(Dispatchers.IO) {
        return@withContext suspendCancellableCoroutine { continuation ->
            firestore.collection(collection)
                .get()
                .addOnSuccessListener { result ->
                    val items = mutableListOf<T>()
                    for (item in result) {
                        val castedItem = item.toObject(T::class.java) as T
                        items.add(castedItem)
                    }
                    continuation.resume(items)
                }
                .addOnFailureListener { exception ->
                    continuation.resumeWithException(exception)
                }
        }
    }

    suspend inline fun <reified T> addDocument(collection: String, documentId: String, data: T): Boolean = withContext(Dispatchers.IO) {
        return@withContext suspendCancellableCoroutine { continuation ->
            firestore.collection(collection)
                .document(documentId)
                .set(data!!)
                .addOnSuccessListener { _ ->
                    continuation.resume(true)
                }
                .addOnFailureListener { exception ->
                    continuation.resumeWithException(exception)
                }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun getSalesWithDetails(userId: String) = withContext<Any?>(Dispatchers.IO) {
        return@withContext suspendCancellableCoroutine { continuation ->
            firestore.collection(SALES_COLLECTION)
                .whereEqualTo("usuarioId", userId)
                .whereGreaterThanOrEqualTo("fecha", fechaLimite)
                .get()
                .addOnSuccessListener { ventasSnapshot ->
                    ventasSnapshot.documents.map { it.toObject(VentaResponse::class.java)}
                        .let { persistData!!.persistVentas(it) }
                    for (item in ventasSnapshot) {
                        firestore.collection(SALES_DETAIL_COLLECTION)
                            .whereEqualTo("ventaId", item.data["id"])
                            .get()
                            .addOnSuccessListener { ventasDetalleSnapshot ->
                                ventasDetalleSnapshot.documents.map {
                                    it.toObject(VentasDetalleResponse::class.java)
                                }.let { persistData!!.persistVentaDetalles(it) }
                            }
                            .addOnFailureListener { exception ->
                                continuation.resumeWithException(exception)
                            }
                    }
                    continuation.resume(null)
                }
                .addOnFailureListener { exception ->
                    continuation.resumeWithException(exception)
                }
        }
    }


}