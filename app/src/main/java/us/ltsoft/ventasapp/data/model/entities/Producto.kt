package us.ltsoft.ventasapp.data.model.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "productos")
class Producto(
    @PrimaryKey(autoGenerate = false)
    var id: String,
    var categoriaId: String,
    var producto: String,
    var habilitado: Boolean,
    var sincronizado: Boolean = false
)