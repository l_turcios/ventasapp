package us.ltsoft.ventasapp.data.model.db

import android.annotation.SuppressLint
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import us.ltsoft.ventasapp.data.model.dao.ICategoriasDao
import us.ltsoft.ventasapp.data.model.dao.IClientesDao
import us.ltsoft.ventasapp.data.model.dao.IEscalasDao
import us.ltsoft.ventasapp.data.model.dao.IProductoDao
import us.ltsoft.ventasapp.data.model.dao.IProductoPreciosDao
import us.ltsoft.ventasapp.data.model.dao.IVentasDao
import us.ltsoft.ventasapp.data.model.dao.IVentasDetalle

@TypeConverters(us.ltsoft.ventasapp.data.model.entities.converter.Converters::class)
@Database(
    entities = [
        us.ltsoft.ventasapp.data.model.entities.Categoria::class,
        us.ltsoft.ventasapp.data.model.entities.Cliente::class,
        us.ltsoft.ventasapp.data.model.entities.Escala::class,
        us.ltsoft.ventasapp.data.model.entities.Producto::class,
        us.ltsoft.ventasapp.data.model.entities.ProductoPrecio::class,
        us.ltsoft.ventasapp.data.model.entities.Venta::class,
        us.ltsoft.ventasapp.data.model.entities.VentasDetalle::class
    ], version = 1, exportSchema = false
)

abstract class VentasDB: RoomDatabase() {
    abstract fun getCategoriasDao(): ICategoriasDao
    abstract fun getClientesDao(): IClientesDao
    abstract fun getEscalasDao(): IEscalasDao
    abstract fun getProductosDao(): IProductoDao
    abstract fun getProductoPreciosDao(): IProductoPreciosDao
    abstract fun getVentasDao(): IVentasDao
    abstract fun getVentasDetalleDao(): IVentasDetalle

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var context: Context

        fun initialize(context: Context){
            this.context = context.applicationContext
        }

        private val database: VentasDB by lazy(LazyThreadSafetyMode.SYNCHRONIZED){
            Room.databaseBuilder(context, VentasDB::class.java, "ventas.db")
                .allowMainThreadQueries()
                .build()
        }

        fun getInstance(): VentasDB{
            return database
        }

        fun destroyInstance(){
            database.close()
        }

        fun clearAllTables(){
            database.clearAllTables()
        }

    }
}