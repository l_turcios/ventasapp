package us.ltsoft.ventasapp.data.model.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "clientes")
class Cliente (
    @PrimaryKey(autoGenerate = false)
    var id: String,
    var nombre: String,
    var telefono: String?,
    var escalaId: String,
    var sincronizado: Boolean = false
)