package us.ltsoft.ventasapp.data.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import us.ltsoft.ventasapp.data.model.entities.Venta
import us.ltsoft.ventasapp.data.model.entities.dto.VentaDTO
import java.time.LocalDateTime

@Dao
interface IVentasDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertVenta(venta: Venta)

    @Query("select * from ventas")
    fun getAllVentas(): List<Venta>

    @Query("delete from ventas")
    fun deleteAllVentas()

    @Query("select * from ventas where id= :id")
    fun getVentaById(id: String): Venta

    @Query("select v.id AS venta, v.fecha, v.total, c.nombre AS cliente from ventas v " +
            "LEFT JOIN clientes c ON c.id == v.clienteId")
    fun getVentas(): List<VentaDTO>

    @Query("select v.id AS venta, v.fecha, v.total, c.nombre AS cliente, c.id AS clienteId, c.escalaId from ventas v " +
            "LEFT JOIN clientes c ON c.id == v.clienteId WHERE v.estado = 'En proceso' " +
            "ORDER BY v.fecha DESC LIMIT 1")
    fun getVentaEnProceso(): VentaDTO?

    @Query("select v.id AS venta, v.fecha, v.total, c.nombre AS cliente, c.id AS clienteId, c.escalaId from ventas v " +
            "LEFT JOIN clientes c ON c.id == v.clienteId WHERE v.id = :id")
    fun getVentaEnProcesoDTOById(id: String): VentaDTO?

    @Query("select v.id AS venta, v.fecha, v.total, c.nombre AS cliente, c.id AS clienteId, c.escalaId from ventas v " +
            "LEFT JOIN clientes c ON c.id == v.clienteId WHERE SUBSTR(v.fecha, 1, 10) =:fecha  " +
            "ORDER BY v.fecha ")
    fun getVentasDTOByFecha(fecha: String): List<VentaDTO>

    fun updateTotalVenta(id: String, total: Double) {
        val venta = getVentaById(id)
        venta.total = total
        upsertVenta(venta)
    }
    fun cerrarVenta(id: String): Boolean {
        val venta = getVentaById(id)
        if (venta.estado == "En proceso") {
            venta.estado = "Cerrada"
            upsertVenta(venta)
            return true
        }
        return false
    }

    fun sincronizadaVenta(id: String): Boolean {
        val venta = getVentaById(id)
        venta.sincronizado = true
        upsertVenta(venta)
        return true
    }

}