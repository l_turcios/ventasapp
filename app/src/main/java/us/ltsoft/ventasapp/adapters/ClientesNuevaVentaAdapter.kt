package us.ltsoft.ventasapp.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO

class ClientesNuevaVentaAdapter(private var clientes: List<ClienteDTO>):
    RecyclerView.Adapter<ClientesNuevaVentaAdapter.ViewHolder>(), Filterable {

    var clientesFiltrados: List<ClienteDTO> = clientes.toList()
    private var onClienteClickListener: OnClienteClickListener? = null
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textCliente: TextView = view.findViewById(R.id.textNombreClienteNuevaVenta)
        val textTipoCliente: TextView = view.findViewById(R.id.textTipoClienteNuevaVenta)

        init {
            view.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION)
                    onClienteClickListener?.onClienteClick(clientesFiltrados[position])
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cliente_nueva_venta, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cliente = clientesFiltrados[position]
        holder.textCliente.text = cliente.nombre.uppercase()
        holder.textTipoCliente.text = cliente.tipo.uppercase()

        holder.itemView.setOnClickListener {
            onClienteClickListener?.onClienteClick(cliente)
        }
    }

    override fun getItemCount(): Int {
        return clientesFiltrados.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filtro = constraint.toString().trim().uppercase()
                clientesFiltrados = if (filtro.isEmpty()) {
                    clientes.toList()
                } else {
                    clientes.filter {
                        it.nombre.uppercase().contains(filtro)
                    }.toList()
                }
                val filterResults = FilterResults()
                filterResults.values = clientesFiltrados
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                clientesFiltrados = results?.values as? List<ClienteDTO> ?: emptyList()
                notifyDataSetChanged()
            }
        }
    }

    fun setOnClienteClickListener(listener: OnClienteClickListener) {
        onClienteClickListener = listener
    }

}

interface OnClienteClickListener {
    fun onClienteClick(clienteSeleccionado: ClienteDTO)
}