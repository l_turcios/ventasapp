package us.ltsoft.ventasapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.VentaDTO

class VentasAdapter(private val ventas: List<VentaDTO>): RecyclerView.Adapter<VentasAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val textCliente: TextView = view.findViewById(R.id.textCliente)
        val textFecha: TextView = view.findViewById(R.id.textFechaHora)
        val textTotal: TextView = view.findViewById(R.id.textTotal)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_venta, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val venta = ventas[position]
        holder.textCliente.text = venta.cliente ?: "Sin cliente"
        holder.textFecha.text = venta.fechaString ?: "Sin fecha"
        holder.textTotal.text = String.format("%.2f", venta.total)
        holder.textTotal.tag = venta.venta
    }

    override fun getItemCount(): Int {
        return ventas.size
    }
}