package us.ltsoft.ventasapp.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.ProductoDTO

class ProductosNuevaVentaAdapter(private var productos: List<ProductoDTO>):
        RecyclerView.Adapter<ProductosNuevaVentaAdapter.ViewHolder>(), Filterable {

    var productosFiltrados: List<ProductoDTO> = productos.toList()

    private var selectedItemPosition: Int = RecyclerView.NO_POSITION
    private var onProductoClickListener: OnProductoClickListener? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textProducto: TextView = view.findViewById(R.id.textProductoNuevaVenta)
        val textCategoria: TextView = view.findViewById(R.id.textCategoriaProductoNuevaVenta)

        init {
            view.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    notifyItemChanged(selectedItemPosition)
                    selectedItemPosition = position
                    notifyItemChanged(selectedItemPosition)
                    onProductoClickListener?.onProductoClick(productosFiltrados[position])
                    view.isSelected = true
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_producto_nueva_venta, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val producto = productosFiltrados[position]
        holder.textProducto.text = producto.producto.uppercase()
        holder.textCategoria.text = producto.categoria.uppercase()
        holder.itemView.isSelected = position == selectedItemPosition
    }

    override fun getItemCount(): Int {
        return productosFiltrados.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filtro = constraint.toString().trim().uppercase()
                selectedItemPosition = RecyclerView.NO_POSITION
                productosFiltrados = if (filtro.isEmpty()) {
                    productos.toList()
                } else {
                    productos.filter {
                        it.producto.uppercase().contains(filtro) ||
                                it.categoria.uppercase().contains(filtro)
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = productosFiltrados
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                productosFiltrados = results?.values as? List<ProductoDTO> ?: emptyList()
                notifyDataSetChanged()
            }
        }
    }

    fun setOnProductoClickListener(listener: OnProductoClickListener) {
        onProductoClickListener = listener
    }
}

interface OnProductoClickListener {
    fun onProductoClick(producto: ProductoDTO)
}
