package us.ltsoft.ventasapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.ProductoDTO

class ProductosFragmentProductosAdapter(private val context: Context) :
    RecyclerView.Adapter<ProductosFragmentProductosAdapter.ViewHolder>(), Filterable{
        private var productos: List<ProductoDTO> = emptyList()
        private var productosFiltrados: List<ProductoDTO> = emptyList()
        private var onProductoClickListener: OnProductoIntoFragmentClickListener? = null

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val textProducto: TextView = view.findViewById(R.id.textNombreProductoFragmentProductos)
        val textCategoria: TextView = view.findViewById(R.id.textCategoriaFragmentProductos)
        init {
            view.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION)
                    onProductoClickListener?.onProductoClick(productosFiltrados[position])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_producto_fragment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ProductosFragmentProductosAdapter.ViewHolder,
        position: Int
    ) {
        val producto = productosFiltrados[position]
        holder.textProducto.text = producto.producto.uppercase()
        holder.textCategoria.text = producto.categoria.uppercase()
        holder.itemView.setOnClickListener {
            onProductoClickListener?.onProductoClick(producto)
        }
    }

    override fun getItemCount(): Int {
        return productosFiltrados.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filtro = constraint.toString().trim().uppercase()
                productosFiltrados = if (filtro.isEmpty()) {
                    productos
                } else {
                    productos.filter {
                        it.producto.uppercase().contains(filtro) ||
                        it.categoria.uppercase().contains(filtro)
                    }
                }
                val results = FilterResults()
                results.values = productosFiltrados
                return results
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                productosFiltrados = results?.values as? List<ProductoDTO> ?: emptyList()
                notifyDataSetChanged()
            }
        }
    }

    fun setOnProductoClickListener(listener: OnProductoIntoFragmentClickListener) {
        onProductoClickListener = listener
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setProductos(productosList: List<ProductoDTO>?) {
        productos = productosList ?: emptyList()
        productosFiltrados = productos
        notifyDataSetChanged()
    }
}

interface OnProductoIntoFragmentClickListener {
    fun onProductoClick(producto: ProductoDTO)
}
