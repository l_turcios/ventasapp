package us.ltsoft.ventasapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.VentasDetalleDTO

class VentasDetalleAdapter(private val context: Context): RecyclerView.Adapter<VentasDetalleAdapter.ViewHolder>() {

    private var ventasDetalle: List<VentasDetalleDTO> = emptyList()

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val textProducto: TextView = view.findViewById(R.id.textProducto)
        val textCategoria: TextView = view.findViewById(R.id.textCategoria)
        val textCantidad: TextView = view.findViewById(R.id.textCantidad)
        val textPrecio: TextView = view.findViewById(R.id.textPrecioProducto)
        val textTotal: TextView = view.findViewById(R.id.textTotalProducto)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_venta_detalle, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ventaDetalle = ventasDetalle[position]
        holder.textProducto.text = ventaDetalle.producto
        holder.textCategoria.text = ventaDetalle.categoria
        holder.textCantidad.text = ventaDetalle.cantidad.toString()
        holder.textPrecio.text = String.format("%.2f", ventaDetalle.precio)
        holder.textTotal.text = String.format("%.2f", ventaDetalle.total)
    }

    override fun getItemCount(): Int {
        return ventasDetalle.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setVentasDetalle(ventasDetalleList: List<VentasDetalleDTO>) {
        ventasDetalle = ventasDetalleList
        notifyDataSetChanged()
    }


}