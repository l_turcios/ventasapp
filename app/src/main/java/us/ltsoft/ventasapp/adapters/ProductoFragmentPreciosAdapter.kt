package us.ltsoft.ventasapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.PrecioDTO

class ProductoFragmentPreciosAdapter(private val context: Context) :
    RecyclerView.Adapter<ProductoFragmentPreciosAdapter.ViewHolder>(){

        private var precios: List<PrecioDTO> = emptyList()
        private var preciosFiltrados: List<PrecioDTO> = emptyList()
        private var onPrecioClickListener: OnPrecioIntoFragmentClickListener? = null

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val textEscala: TextView = view.findViewById(R.id.textEscalaItem)
        val textRangoVolumen: TextView = view.findViewById(R.id.textRangoItem)
        val textPrecio: TextView = view.findViewById(R.id.textPrecioItem)

        init {
            view.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION)
                    onPrecioClickListener?.onPrecioClick(preciosFiltrados[position])
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_precio_producto, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductoFragmentPreciosAdapter.ViewHolder, position: Int) {
        val precio = preciosFiltrados[position]
        holder.textEscala.text = precio.escala.uppercase()
        holder.textRangoVolumen.text = String.format("%s - %s", precio.cantMin, precio.cantMax)
        holder.textPrecio.text = String.format("$%.2f", precio.precio)
        holder.itemView.setOnClickListener {
            onPrecioClickListener?.onPrecioClick(precio)
        }
    }

    override fun getItemCount(): Int {
        return preciosFiltrados.size
    }

    fun setOnPrecioClickListener(listener: OnPrecioIntoFragmentClickListener) {
        onPrecioClickListener = listener
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPrecios(preciosList: List<PrecioDTO>) {
        precios = preciosList
        preciosFiltrados = preciosList
        notifyDataSetChanged()
    }

}

interface OnPrecioIntoFragmentClickListener {
    fun onPrecioClick(precio: PrecioDTO)
}
