package us.ltsoft.ventasapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import us.ltsoft.ventasapp.R
import us.ltsoft.ventasapp.data.model.entities.dto.ClienteDTO

class ClientesFragmentClientesAdapter(private val context: Context) :
    RecyclerView.Adapter<ClientesFragmentClientesAdapter.ViewHolder>(), Filterable {

    private var clientes: List<ClienteDTO> = emptyList()
    var clientesFiltrados: List<ClienteDTO> = emptyList()
    private var onClienteClickListener: OnClienteIntoFragmentClickListener? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textCliente: TextView = view.findViewById(R.id.textNombreClienteFragmentClientes)
        val textTipoCliente: TextView = view.findViewById(R.id.textTipoClienteFragmentClientes)
        val telefonoCliente: TextView = view.findViewById(R.id.textTelefonoClienteFragmentClientes)
        init {
            view.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION)
                    onClienteClickListener?.onClienteClick(clientesFiltrados[position])
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cliente_fragment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ClientesFragmentClientesAdapter.ViewHolder,
        position: Int
    ) {
        val cliente = clientesFiltrados[position]
        holder.textCliente.text = cliente.nombre.uppercase()
        holder.textTipoCliente.text = cliente.tipo.uppercase()
        holder.telefonoCliente.text = cliente.telefono
        holder.itemView.setOnClickListener {
            onClienteClickListener?.onClienteClick(cliente)
        }
    }

    override fun getItemCount(): Int {
        return clientesFiltrados.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filtro = constraint.toString().trim().uppercase()
                clientesFiltrados = if (filtro.isEmpty()) {
                    clientes.toList()
                } else {
                    clientes.filter {
                        it.nombre.uppercase().contains(filtro)
                    }
                }
                val results = FilterResults()
                results.values = clientesFiltrados
                return results
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                clientesFiltrados = results?.values as? List<ClienteDTO> ?: emptyList()
                notifyDataSetChanged()
            }
        }
    }

    fun setOnClienteClickListener(listener: OnClienteIntoFragmentClickListener) {
        onClienteClickListener = listener
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setClientes(clientesList: List<ClienteDTO>?) {
        clientes = clientesList ?: emptyList()
        clientesFiltrados = clientes
        notifyDataSetChanged()
    }

}

interface OnClienteIntoFragmentClickListener {
    fun onClienteClick(cliente: ClienteDTO)
}